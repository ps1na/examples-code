<?php

namespace App\Factory;

use App\Actions\CheckDocuments;
use App\Actions\CheckMarketingDocuments;
use App\Actions\CheckQTB;
use App\Actions\Close;
use App\Actions\CloseNotReady;
use App\Actions\CompensationMoney;
use App\Actions\CompensationTire;
use App\Actions\DeliverQTB;
use App\Actions\DeliverStock;
use App\Actions\GCCheck;
use App\Actions\GCCompensationMoney;
use App\Actions\MarketingClose;
use App\Actions\PrepareTransportation;
use App\Actions\AbstractAction;
use App\Actions\ActionInterface;
use App\Actions\RequestTireReturn;
use App\Actions\RequestTransportation;
use App\Actions\ReturnTireToActualDateCustomer;
use App\Actions\ReturnTireToCustomer;
use App\Actions\ReturnTireToStock;
use App\Actions\SendNewTire;
use App\Actions\SendToQTB;
use App\Entity\Reclamation;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Workflow\Transition;

class ReclamationActionsFactory
{
    /**
     * @var User|null
     */
    private $user;

    /**
     * @var string[]
     */
    private $actionsMap;

    /**
     * @var ActionInterface[][]
     */
    private $actions;

    /**
     * ReclamationActionsFactory constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        if ($tokenStorage->getToken()) {
            $this->user = $tokenStorage->getToken()->getUser();
        }

        $this->actionsMap = array();
        $this
            ->registerAction(CheckDocuments::class)
            ->registerAction(CheckQTB::class)
            ->registerAction(Close::class)
            ->registerAction(CloseNotReady::class)
            ->registerAction(CompensationMoney::class)
            ->registerAction(CompensationTire::class)
            ->registerAction(DeliverQTB::class)
            ->registerAction(DeliverStock::class)
            ->registerAction(PrepareTransportation::class)
            ->registerAction(RequestTireReturn::class)
            ->registerAction(RequestTransportation::class)
            ->registerAction(ReturnTireToCustomer::class)
            ->registerAction(ReturnTireToStock::class)
            ->registerAction(SendNewTire::class)
            ->registerAction(SendToQTB::class)
            ->registerAction(MarketingClose::class)
            ->registerAction(CheckMarketingDocuments::class)
            ->registerAction(ReturnTireToActualDateCustomer::class)
            ->registerAction(GCCheck::class)
            ->registerAction(GCCompensationMoney::class)
        ;
    }

    /**
     * @param string $action
     * @return $this
     */
    protected function registerAction(string $action)
    {
        if (is_subclass_of($action, ActionInterface::class)) {
            /**
             * @var ActionInterface $action
             */
            $transitions = $action::getTransitions();
            foreach ($transitions as $transition) {
                $this->actionsMap[$transition] = $action;
            }
        }
        return $this;
    }

    /**
     * @param Reclamation $reclamation
     * @param Transition $transition
     * @return AbstractAction|null
     */
    public function createAction(Reclamation $reclamation, Transition $transition): ?AbstractAction
    {
        if (isset($this->actionsMap[$transition->getName()])) {
            $class = $this->actionsMap[$transition->getName()];
            $name = $class::getName();
            if (!isset($this->actions[$name])) {
                $this->actions[$name] = [];
            }
            if (!isset($this->action[$name][$reclamation->getId()])) {
                $this->actions[$name][$reclamation->getId()] = new $class($reclamation);
            }
            return $this->actions[$name][$reclamation->getId()];
        }
        return null;
    }
}