<?php

namespace App\Repository\ItemList;

use App\Dto\Reclamation\Filter\DefaultFilterDto;
use App\Dto\Reclamation\Filter\KgshFilterDto;
use App\Dto\Reclamation\Filter\MarketingFilterDto;
use App\Entity\Reclamation;
use App\Entity\State;
use App\Enum\ReclamationState;
use App\Form\Type\Reclamation\Filter\MarketingFilterType;
use App\Service\ItemList\Filter\FilterInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;

class ReclamationListRepository extends AbstractListRepository
{
    /**
     * ReclamationListRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reclamation::class);
    }

    /**
     * @return QueryBuilder
     */
    protected function createDefaultBuilder(): QueryBuilder
    {
        return $this->createQueryBuilder('r')
            ->leftJoin('r.reclamationProduct', 'reclamationProduct')
            ->leftJoin('r.dealer', 'dealer')
        ;
    }

    /**
     * TODO: refactor
     * @param FilterInterface $filter
     * @param QueryBuilder $builder
     * @return mixed|void
     */
    protected function applyFilter(FilterInterface $filter, QueryBuilder $builder)
    {
        if ($filter->reclamationTypes) {
            $typeFilters = array();
            $params = array();
            // type = FT/SW....
            foreach ($filter->reclamationTypes as $key => $type) {
                $typeFilters[] = $builder
                    ->expr()->like('r.number', ":type_$key");
                $params["type_$key"] = $type . '%';
            }
            $builder
                ->andWhere(new Expr\Orx(
                    $typeFilters
                ))
            ;
            foreach ($params as $paramName => $param) {
                $builder
                    ->setParameter($paramName, $param)
                ;
            }
        }

        if ($filter instanceof DefaultFilterDto) {
            if ($filter->dealer) {
                $builder
                    ->andWhere('r.dealer = :dealer')
                    ->setParameter('dealer', $filter->dealer)
                ;
            }
            if ($filter->state) {
                $builder
                    ->andWhere('r.state = :state')
                    ->setParameter('state', $filter->state)
                ;
            }
            if ($filter->number) {
                $builder
                    ->andWhere('r.number LIKE :number333')
                    ->setParameter('number333', '%' . $filter->number . '%')
                ;
            }

            if ($filter->processingTime) {
                $currentDate = $filter->processingTime->setTime(0, 0);
                $nextDate = (clone $currentDate)->add(\DateInterval::createFromDateString('1 day'));
                $builder
                    ->andWhere('r.processingTime >= :currentDate')
                    ->andWhere('r.processingTime <= :nextDate')
                    ->setParameter('currentDate', $currentDate)
                    ->setParameter('nextDate', $nextDate)
                ;
            }

            if ($filter->compensation) {
                $builder
                    ->andWhere('r.compensation = :compensation')
                    ->setParameter('compensation', $filter->compensation)
                ;
            }

            if ($filter->transportType) {
                $builder
                    ->andWhere('r.transportType = :transportType')
                    ->setParameter('transportType', $filter->transportType)
                ;
            }
        }

        if ($filter instanceof MarketingFilterDto || $filter instanceof KgshFilterDto) {
            if ($filter->number) {
                $builder
                    ->andWhere('r.number LIKE :number')
                    ->setParameter('number', '%' . $filter->number . '%')
                ;
            }
            if ($filter->processingTime) {
                $currentDate = $filter->processingTime->setTime(0, 0);
                $nextDate = (clone $currentDate)->add(\DateInterval::createFromDateString('1 day'));
                $builder
                    ->andWhere('r.processingTime >= :currentDate')
                    ->andWhere('r.processingTime <= :nextDate')
                    ->setParameter('currentDate', $currentDate)
                    ->setParameter('nextDate', $nextDate)
                ;
            }

            if ($filter->dealer) {
                $builder
                    ->andWhere('r.dealer = :dealer')
                    ->setParameter('dealer', $filter->dealer)
                ;
            }

            if ($filter->email) {
                $builder
                    ->andWhere('r.tyreUser.email = :email')
                    ->setParameter('email', $filter->email)
                ;
            }
        }

        if ($filter instanceof MarketingFilterDto) {
            if ($filter->dateBuy) {
                $currentDate = $filter->dateBuy->setTime(0, 0);
                $nextDate = (clone $currentDate)->add(\DateInterval::createFromDateString('1 day'));
                $builder
                    ->andWhere('reclamationProduct.dateBuy >= :currentDate')
                    ->andWhere('reclamationProduct.dateBuy <= :nextDate')
                    ->setParameter('currentDate', $currentDate)
                    ->setParameter('nextDate', $nextDate)
                ;
            }

            if ($filter->programType) {
                $builder
                    ->andWhere('dealer.cooperationProgram = :programType')
                    ->setParameter('programType', $filter->programType)
                ;
            }

            if ($filter->tyreMark) {
                $builder
                    ->andWhere('reclamationProduct.tyreMark = :tyreMark')
                    ->setParameter('tyreMark', $filter->tyreMark)
                ;
            }

            if ($filter->tyreModel) {
                $builder
                    ->andWhere('reclamationProduct.tyreModel = :tyreModel')
                    ->setParameter('tyreModel', $filter->tyreModel)
                ;
            }

            if ($filter->tyreSize) {
                $builder
                    ->andWhere('reclamationProduct.tyreSize = :tyreSize')
                    ->setParameter('tyreSize', $filter->tyreSize)
                ;
            }

            if ($filter->tyreIndex) {
                $builder
                    ->andWhere('reclamationProduct.tyreIndex = :tyreIndex')
                    ->setParameter('tyreIndex', $filter->tyreIndex)
                ;
            }

            if ($filter->tyreIndex) {
                $builder
                    ->andWhere('reclamationProduct.dot LIKE :dot')
                    ->setParameter('dot', '%' . $filter->dot . '%')
                ;
            }

            if ($filter->ogp) {
                $builder
                    ->andWhere('reclamationProduct.OGP LIKE :ogp')
                    ->setParameter('ogp', '%' . $filter->ogp . '%')
                ;
            }

            if ($filter->damagePlace) {
                $builder
                    ->andWhere('reclamationProduct.damagePlace = :damagePlace')
                    ->setParameter('damagePlace', $filter->damagePlace)
                ;
            }

            if ($filter->damageCharacter) {
                $builder
                    ->andWhere('reclamationProduct.damageCharacter = :damageCharacter')
                    ->setParameter('damageCharacter', $filter->damageCharacter)
                ;
            }

            if ($filter->solution) {
                $builder
                    ->andWhere('reclamationProduct.solution LIKE :solution')
                    ->setParameter('solution', '%' . $filter->solution . '%')
                ;
            }

            if (null !== $filter->qtbCheck) {
                if ($filter->qtbCheck) {
                    $builder
                        ->innerJoin(
                            'r.stateChanges',
                            'changes',
                            Expr\Join::WITH,
                            $builder->expr()->orX(
                                $builder->expr()->eq('changes.stateTo',':technical_compensation'),
                                $builder->expr()->eq('changes.stateTo',':commercial_compensation')
                            )
                        )
                        ->setParameter('technical_compensation', $builder
                            ->getEntityManager()
                            ->getRepository(State::class)
                            ->find(ReclamationState::TECHNICAL_COMPENSATION)
                        )
                        ->setParameter('commercial_compensation', $builder
                            ->getEntityManager()
                            ->getRepository(State::class)
                            ->find(ReclamationState::COMMERCE_COMPENSATION)
                        )
                    ;
                } else {
                    $builder
                        ->innerJoin(
                            'r.stateChanges',
                            'changes',
                            Expr\Join::WITH,
                            $builder->expr()->eq('changes.stateTo',':rejected')
                        )
                        ->setParameter('rejected', $builder
                            ->getEntityManager()
                            ->getRepository(State::class)
                            ->find(ReclamationState::REJECTED)
                        )
                    ;
                }
            }
            if (null !== $filter->docsCheck) {
                if ($filter->docsCheck) {
                    $builder
                        ->innerJoin(
                            'r.stateChanges',
                            'changes',
                            Expr\Join::WITH,
                            $builder->expr()->eq('changes.stateTo',':correct_docs')
                        )
                        ->setParameter('correct_docs', $builder
                            ->getEntityManager()
                            ->getRepository(State::class)
                            ->find(ReclamationState::CORRECT_DOCS)
                        )
                    ;
                } else {
                    $builder
                        ->innerJoin(
                            'r.stateChanges',
                            'changes',
                            Expr\Join::WITH,
                            $builder->expr()->eq('changes.stateTo',':incorrect_docs')
                        )
                        ->setParameter('incorrect_docs', $builder
                            ->getEntityManager()
                            ->getRepository(State::class)
                            ->find(ReclamationState::INCORRECT_DOCS)
                        )
                    ;
                }
            }
            if ($filter->dateLastStatus) {
                $currentDate = $filter->dateBuy->setTime(0, 0);
                $nextDate = (clone $currentDate)->add(\DateInterval::createFromDateString('1 day'));
                $builder
                    ->innerJoin(
                        'r.stateChanges',
                        'changes',
                        Expr\Join::WITH,
                        $builder->expr()->andX(
                            $builder->expr()->in('changes.stateTo',':statuses'),
                            $builder->expr()->gte('changes.datetime',':currentDate'),
                            $builder->expr()->lte('changes.datetime',':nextDate')
                        )
                    )
                    ->setParameter('statuses',
                        $builder
                            ->getEntityManager()
                            ->getRepository(State::class)
                            ->find(array(
                                ReclamationState::CORRECT_DOCS,
                                ReclamationState::INCORRECT_DOCS,
                                ReclamationState::REJECTED,
                                ReclamationState::TECHNICAL_COMPENSATION,
                                ReclamationState::COMMERCE_COMPENSATION,
                            ))
                    )
                    ->setParameter('currentDate', $currentDate)
                    ->setParameter('nextDate', $nextDate)
                ;
            }

            if (null !== $filter->costOfWorkRepair) {
                $builder
                    ->andWhere('reclamationProduct.costOfWorkRepair = :costOfWorkRepair')
                    ->setParameter('costOfWorkRepair', $filter->costOfWorkRepair)
                ;
            }

            if (null !== $filter->costOfWorkMounting) {
                $builder
                    ->andWhere('reclamationProduct.costOfWorkMounting = :costOfWorkMounting')
                    ->setParameter('costOfWorkRepair', $filter->costOfWorkMounting)
                ;
            }
            if (null !== $filter->costReplaceTire) {
                $builder
                    ->andWhere('reclamationProduct.costReplaceTire = :costReplaceTire')
                    ->setParameter('costReplaceTire', $filter->costReplaceTire)
                ;
            }

            if (null !== $filter->reasonReclamation) {
                $builder
                    ->andWhere('reclamationProduct.reasonReclamation LIKE :reasonReclamation')
                    ->setParameter('reasonReclamation', '%' . $filter->reasonReclamation . '%')
                ;
            }
            if (null !== $filter->commentReclamation) {
                $builder
                    ->andWhere('r.commentReclamation LIKE :commentReclamation')
                    ->setParameter('commentReclamation', '%' . $filter->commentReclamation . '%')
                ;
            }
        }
    }




}