<?php

namespace App\Repository;

use App\Dto\ConsolidatedReportDto;
use App\Entity\Dealer;
use App\Entity\PaginationLinks;
use App\Entity\Reclamation;
use App\Entity\ReclamationList;
use App\Entity\ReportStatus;
use App\Entity\StateChange;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr;

/**
 * Class ReclamationRepository
 * @package App\Repository
 */
class ReclamationRepository extends ServiceEntityRepository
{
    use FiltersRepository;

    /**
     * @var StateChangeRepository
     */
    private $stateChangeRepository;

    /**
     * ReclamationRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry, StateChangeRepository $stateChangeRepository)
    {
        parent::__construct($registry, Reclamation::class);
        $this->stateChangeRepository = $stateChangeRepository;
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @return Collection|Reclamation[]
     */
    public function findBetweenDates(\DateTime $from, \DateTime $to)
    {
        return $this->createQueryBuilder('reclamation')
            ->andWhere('reclamation.importDate >= :date_from')
            ->setParameter('date_from', $from->setTime(0,0,0))
            ->andWhere('reclamation.importDate <= :date_to')
            ->setParameter('date_to', $to->setTime(23,59,59))
            ->getQuery()
            ->getResult()
        ;
    }


    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @return Collection|Reclamation[]
     */
    public function findReclamationType(User $user,\DateTime $from, \DateTime $to)
    {
        $filterparent=[];
        foreach ($user->getParentreclamations() as $types)
        {
            $filterparent[]=$types->getPrefix();
        }

            $builder= $this->createQueryBuilder('reclamation');
            $aliases = $builder->getRootAliases();
            $alias = reset($aliases);
            $typeFilters = array();
            $params = array();
            // type = FT/SW....
            foreach ($filterparent as $key => $type) {
                $typeFilters[] = $builder
                    ->expr()->like($alias . '.number', ":type_$key");
                $params["type_$key"] = $type . '%';
            }
            $builder

                ->andWhere(new Expr\Orx(
                    $typeFilters
                ))
                ->andWhere('reclamation.importDate >= :date_from')
                ->setParameter('date_from', $from->setTime(0,0,0))
                ->andWhere('reclamation.importDate <= :date_to')
                ->setParameter('date_to', $to->setTime(23,59,59))
                  ;
            foreach ($params as $paramName => $param) {
                $builder
                    ->setParameter($paramName, $param)
                ;
            }


        return $builder->getQuery()->getResult();
    }


    /**
     * @param ReportStatus[] $resultStates
     * @return ReportStatus[]
     */
    public function findReclamationForStatusReport($resultStates)
    {
        $newState = [];
        /**
         * @var ReportStatus $state
         */
        foreach ($resultStates as $state) {
            $countTC = count($this->createQueryBuilder('reclamation')
                ->where('
                (reclamation.importDate < :now AND reclamation.state = :state_id) 
                    AND 
                (
                (reclamation.typeReclamation = :type_TC AND reclamation.number LIKE :number_TC)
                 OR 
                reclamation.typeReclamation = :type_BF 
                 OR 
                reclamation.typeReclamation = :type_MS 
                 OR 
                reclamation.typeReclamation = :type_SW
                )')
                ->setParameter('now', new \DateTime('now'))
                ->setParameter('state_id', $state->getIdState())
                ->setParameter('type_TC', '1')
                ->setParameter('type_BF', '3')
                ->setParameter('type_MS', '4')
                ->setParameter('type_SW', '5')
                ->setParameter('number_TC', 'TC%')
                ->getQuery()->execute());
            $state->setCountTC($countTC);
            $countPL = count($this->createQueryBuilder('reclamation')
                ->where('
                (reclamation.importDate < :now AND reclamation.state = :state_id) 
                    AND 
                (
                (reclamation.typeReclamation = :type_TC AND reclamation.number LIKE :number_PL)
                 OR 
                reclamation.typeReclamation = :type_GC 
                 OR 
                reclamation.typeReclamation = :type_EW 
                 OR 
                reclamation.number LIKE :number_TEST
                )')
                ->setParameter('now', new \DateTime('now'))
                ->setParameter('state_id', $state->getIdState())
                ->setParameter('type_TC', '1')
                ->setParameter('type_GC', '2')
                ->setParameter('type_EW', '6')
                ->setParameter('number_TEST', 'TEST%')
                ->setParameter('number_PL', 'PL%')
                ->getQuery()->execute());
            $state->setCountPL($countPL);
            $countGC = count($this->createQueryBuilder('reclamation')
                ->where('
                (reclamation.importDate < :now AND reclamation.state = :state_id) AND reclamation.number LIKE :number_GC')
                ->setParameter('now', new \DateTime('now'))
                ->setParameter('state_id', $state->getIdState())
                ->setParameter('number_GC', 'GC%')
                ->getQuery()->execute());
            $state->setCountGC($countGC);

            $newState[] = $state;
        }
        return $newState;
    }

    /**
     * @param ReclamationList $reclamationList
     * @return ReclamationList
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findByList(ReclamationList $reclamationList)
    {
        $reclamationList->setOffset(0);
        /**
         * @var Reclamation[] $reclamations
         */
        $reclamations = [];
        $conn = $this->getEntityManager()->getConnection();
        if ($reclamationList->getFilter()->getFieldsValue() === null) {
            $sql = /** @lang php */
                'SELECT * FROM reclamation r WHERE 
                r.typeReclamation = :typeReclamation AND 
                r.DealerId = :dealerId'
            ;
            $stmt = $conn->prepare($sql);
            $stmt->execute(
                [
                    'typeReclamation' => $reclamationList->getFilter()->getType(),
                    'dealerId' => $reclamationList->getDealer()->getId()
                ]
            );
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql = /** @lang php */
                'SELECT * FROM reclamation r WHERE 
                r.typeReclamation = :typeReclamation AND 
                r.DealerId = :dealerId 
                ORDER BY r.ReclamationId DESC 
                LIMIT :limit 
                OFFSET :offset'
            ;
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        if ($reclamationList->getFilter()->getNumberReclamation()) {
            $sql = /** @lang php */
                'SELECT * FROM reclamation r WHERE 
                r.typeReclamation = :typeReclamation AND 
                r.DealerId = :dealerId AND 
                r.Number LIKE :reclamationNumber 
                ORDER BY r.ReclamationId DESC'
            ;
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('reclamationNumber', '%'.$reclamationList->getFilter()->getNumberReclamation().'%');
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql = /** @lang php */
                'SELECT * FROM reclamation r WHERE 
                r.typeReclamation = :typeReclamation AND 
                r.DealerId = :dealerId AND 
                r.Number LIKE :reclamationNumber 
                ORDER BY r.ReclamationId DESC 
                LIMIT :limit 
                OFFSET :offset'
            ;
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('reclamationNumber', '%'.$reclamationList->getFilter()->getNumberReclamation().'%');
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        if ($reclamationList->getFilter()->getProcessingTime()) {
            $sql = /** @lang php */
                'SELECT * FROM reclamation r WHERE 
                r.typeReclamation = :typeReclamation AND 
                r.DealerId = :dealerId AND 
                r.ProcessingTime >= :processingTimeFrom AND
                r.ProcessingTime <= :processingTimeTo 
                ORDER BY r.ReclamationId DESC'
            ;
            $timeFrom = $reclamationList->getFilter()->getProcessingTime();
            $timeTo = $reclamationList->getFilter()->getProcessingTimeTo();
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('processingTimeFrom', $timeFrom, 'datetime');
            $stmt->bindValue('processingTimeTo', $timeTo, 'datetime');
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql = /** @lang php */
                'SELECT * FROM reclamation r WHERE 
                r.typeReclamation = :typeReclamation AND 
                r.DealerId = :dealerId AND 
                r.ProcessingTime >= :processingTimeFrom AND
                r.ProcessingTime <= :processingTimeTo 
                ORDER BY r.ReclamationId DESC 
                LIMIT :limit 
                OFFSET :offset'
            ;
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('processingTimeFrom', $timeFrom, 'datetime');
            $stmt->bindValue('processingTimeTo', $timeTo, 'datetime');
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);

        }

        if ($reclamationList->getFilter()->getFIO()) {
            $sql =  /** @lang php */ 'SELECT * FROM reclamation r 
                LEFT JOIN tyre_user t on r.TyreUserId = t.TyreUserId
                WHERE (t.FirstName LIKE :firstName OR 
                t.MiddleName LIKE :middleName OR 
                t.LastName LIKE :lastName) AND 
                (r.typeReclamation = :typeReclamation AND r.DealerId = :dealerId) 
                ORDER BY r.ReclamationId DESC'
            ;
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('firstName', $reclamationList->getFilter()->getFIO().'%');
            $stmt->bindValue('middleName', $reclamationList->getFilter()->getFIO().'%');
            $stmt->bindValue('lastName', $reclamationList->getFilter()->getFIO().'%');
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql =  /** @lang php */ 'SELECT * FROM reclamation r 
                LEFT JOIN tyre_user t on r.TyreUserId = t.TyreUserId
                WHERE (t.FirstName LIKE :firstName OR 
                t.MiddleName LIKE :middleName OR 
                t.LastName LIKE :lastName) AND 
                (r.typeReclamation = :typeReclamation AND r.DealerId = :dealerId) 
                ORDER BY r.ReclamationId DESC 
                LIMIT :limit 
                OFFSET :offset'
            ;
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('firstName', $reclamationList->getFilter()->getFIO().'%');
            $stmt->bindValue('middleName', $reclamationList->getFilter()->getFIO().'%');
            $stmt->bindValue('lastName', $reclamationList->getFilter()->getFIO().'%');
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);

        }

        if ($reclamationList->getFilter()->getOrganization()) {
            $sql =  /** @lang php */ 'SELECT * FROM reclamation r 
                LEFT JOIN tyre_user t on r.TyreUserId = t.TyreUserId
                WHERE t.Enterprise LIKE :enterprise AND 
                r.DealerId = :dealerId AND 
                r.typeReclamation = :typeReclamation 
                ORDER BY r.ReclamationId DESC'
            ;
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('enterprise', $reclamationList->getFilter()->getOrganization().'%');
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql =  /** @lang php */ 'SELECT * FROM reclamation r 
                LEFT JOIN tyre_user t on r.TyreUserId = t.TyreUserId
                WHERE t.Enterprise LIKE :enterprise AND 
                r.DealerId = :dealerId AND 
                r.typeReclamation = :typeReclamation 
                ORDER BY r.ReclamationId DESC 
                LIMIT :limit 
                OFFSET :offset'
            ;
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('enterprise', $reclamationList->getFilter()->getOrganization().'%');
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        if ($reclamationList->getFilter()->getAddress()) {
            $sql =  /** @lang php */ 'SELECT * FROM reclamation r 
                LEFT JOIN tyre_user t on r.TyreUserId = t.TyreUserId
                WHERE t.Address LIKE :address AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY r.ReclamationId DESC';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('address', '%'.$reclamationList->getFilter()->getAddress().'%');
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql =  /** @lang php */ 'SELECT * FROM reclamation r 
                LEFT JOIN tyre_user t on r.TyreUserId = t.TyreUserId
                WHERE t.Address LIKE :address AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY r.ReclamationId DESC 
                LIMIT :limit 
                OFFSET :offset';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('address', '%'.$reclamationList->getFilter()->getAddress().'%');
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        if ($reclamationList->getFilter()->getSerialNumber()) {
            $sql =  /** @lang php */ 'SELECT * FROM reclamation_product rp 
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE rp.SerialNumber = :serialNumber AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('serialNumber', $reclamationList->getFilter()->getSerialNumber().'%');
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql =  /** @lang php */ 'SELECT * FROM reclamation_product rp 
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE rp.SerialNumber = :serialNumber AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC 
                LIMIT :limit 
                OFFSET :offset';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('serialNumber', $reclamationList->getFilter()->getSerialNumber().'%');
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        if ($reclamationList->getFilter()->getCai()) {
            $sql =  /** @lang php */ 'SELECT * FROM tyre_data td 
                LEFT JOIN reclamation_product rp on td.cai_id = rp.tyre_cai
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE td.cai_title = :cai AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('cai', $reclamationList->getFilter()->getCai());
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql =  /** @lang php */ 'SELECT * FROM tyre_data td 
                LEFT JOIN reclamation_product rp on td.cai_id = rp.tyre_cai
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE td.cai_title = :cai AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC 
                LIMIT :limit 
                OFFSET :offset';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('cai', $reclamationList->getFilter()->getCai());
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        if ($reclamationList->getFilter()->getMark()) {
            $sql =  /** @lang php */ 'SELECT * FROM tyre_data td 
                LEFT JOIN reclamation_product rp on td.brand_id = rp.tyre_mark
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE td.brand_title = :mark AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('mark', $reclamationList->getFilter()->getMark());
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql =  /** @lang php */ 'SELECT * FROM tyre_data td 
                LEFT JOIN reclamation_product rp on td.brand_id = rp.tyre_mark
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE td.brand_title = :mark AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC 
                LIMIT :limit 
                OFFSET :offset';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('mark', $reclamationList->getFilter()->getMark());
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        if ($reclamationList->getFilter()->getModel()) {
            $sql =  /** @lang php */ 'SELECT * FROM tyre_data td 
                LEFT JOIN reclamation_product rp on td.model_id = rp.tail_model
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE td.model_title = :model AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('model', $reclamationList->getFilter()->getModel());
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql =  /** @lang php */ 'SELECT * FROM tyre_data td 
                LEFT JOIN reclamation_product rp on td.model_id = rp.tail_model
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE td.model_title = :model AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC 
                LIMIT :limit 
                OFFSET :offset';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('model', $reclamationList->getFilter()->getModel());
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        if ($reclamationList->getFilter()->getSolution()) {
            $sql =  /** @lang php */ 'SELECT * FROM reclamation_product rp 
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE rp.solution = :solution AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('solution', $reclamationList->getFilter()->getSolution());
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql =  /** @lang php */ 'SELECT * FROM reclamation_product rp 
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE rp.solution = :solution AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC 
                LIMIT :limit 
                OFFSET :offset';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('solution', $reclamationList->getFilter()->getSolution());
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        if ($reclamationList->getFilter()->getCostOfWorkRepair()) {
            $sql =  /** @lang php */ 'SELECT * FROM reclamation_product rp 
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE rp.costOfWorkRepair = :costOfWorkRepair AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('costOfWorkRepair', $reclamationList->getFilter()->getCostOfWorkRepair());
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql =  /** @lang php */ 'SELECT * FROM reclamation_product rp 
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE rp.costOfWorkRepair = :costOfWorkRepair AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC 
                LIMIT :limit 
                OFFSET :offset';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('costOfWorkRepair', $reclamationList->getFilter()->getCostOfWorkRepair());
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        if ($reclamationList->getFilter()->getCostOfWorkMounting()) {
            $sql =  /** @lang php */ 'SELECT * FROM reclamation_product rp 
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE rp.costOfWorkMounting = :costOfWorkMounting AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('costOfWorkMounting', $reclamationList->getFilter()->getCostOfWorkMounting());
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql =  /** @lang php */ 'SELECT * FROM reclamation_product rp 
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE rp.costOfWorkMounting = :costOfWorkMounting AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC 
                LIMIT :limit 
                OFFSET :offset';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('costOfWorkMounting', $reclamationList->getFilter()->getCostOfWorkMounting());
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        if ($reclamationList->getFilter()->getStatus()) {
            $sql =  /** @lang php */ 'SELECT * FROM reclamation r
                LEFT JOIN state s on s.StateId = r.StateId
                WHERE s.Name LIKE :stateName AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY r.ReclamationId DESC';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('stateName', '%'.$reclamationList->getFilter()->getStatus().'%');
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql =  /** @lang php */ 'SELECT * FROM reclamation r
                LEFT JOIN state s on s.StateId = r.StateId
                WHERE s.Name LIKE :stateName AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY r.ReclamationId DESC 
                LIMIT :limit 
                OFFSET :offset';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('stateName', '%'.$reclamationList->getFilter()->getStatus().'%');
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        if ($reclamationList->getFilter()->getAutoPark()) {
            $sql =  /** @lang php */ 'SELECT * FROM reclamation r
                LEFT JOIN tyre_user t on t.TyreUserId = r.TyreUserId
                LEFT JOIN auto_park_tyre_user a on a.id = t.AutoParkTyreUser
                WHERE a.title = :autoPark AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY r.ReclamationId DESC';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('autoPark', $reclamationList->getFilter()->getAutoPark());
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql =  /** @lang php */ 'SELECT * FROM reclamation r
                LEFT JOIN tyre_user t on t.TyreUserId = r.TyreUserId
                LEFT JOIN auto_park_tyre_user a on a.id = t.AutoParkTyreUser
                WHERE a.title = :autoPark AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY r.ReclamationId DESC 
                LIMIT :limit 
                OFFSET :offset';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('autoPark', $reclamationList->getFilter()->getAutoPark());
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        if ($reclamationList->getFilter()->getIsCompensation()) {
            $compensation = $reclamationList->getFilter()->getIsCompensation();
            if ($reclamationList->getFilter()->getIsCompensation() === 'Компенсация') {
                $compensation = 1;
            }

            if ($reclamationList->getFilter()->getIsCompensation() === 'Отказ') {
                $compensation = 0;
            }
            $sql =  /** @lang php */ 'SELECT * FROM reclamation r
                WHERE r.isCompensation = :isCompensation AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY r.ReclamationId DESC';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('isCompensation', $compensation);
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql =  /** @lang php */ 'SELECT * FROM reclamation r
                WHERE r.isCompensation = :isCompensation AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY r.ReclamationId DESC';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('isCompensation', $compensation);
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        if ($reclamationList->getFilter()->getIsCorrectDocs()) {
            $resultDocs = $reclamationList->getFilter()->getIsCompensation();
            if ($reclamationList->getFilter()->getIsCorrectDocs() === 'Корректные') {
                $resultDocs = 1;
            }

            if ($reclamationList->getFilter()->getIsCorrectDocs() === 'Некорректные') {
                $resultDocs = 0;
            }
            $sql =  /** @lang php */ 'SELECT * FROM reclamation r
                WHERE r.isResultDocs = :isResultDocs AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY r.ReclamationId DESC';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('isResultDocs', $resultDocs);
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql =  /** @lang php */ 'SELECT * FROM reclamation r
                WHERE r.isResultDocs = :isResultDocs AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY r.ReclamationId DESC';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('isResultDocs', $resultDocs);
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        if ($reclamationList->getFilter()->getSize()) {
            $sql =  /** @lang php */ 'SELECT * FROM tyre_data td 
                LEFT JOIN reclamation_product rp on td.size_id = rp.tyre_size
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE td.size_title = :size AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('size', $reclamationList->getFilter()->getSize());
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql =  /** @lang php */ 'SELECT * FROM tyre_data td 
                LEFT JOIN reclamation_product rp on td.size_id = rp.tyre_size
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE td.size_title = :size AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC 
                LIMIT :limit 
                OFFSET :offset';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('size', $reclamationList->getFilter()->getSize());
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        if ($reclamationList->getFilter()->getCostReplaceTire()) {
            $sql =  /** @lang php */ 'SELECT * FROM reclamation_product rp 
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE rp.costReplaceTire = :costReplaceTire AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('costReplaceTire', $reclamationList->getFilter()->getCostReplaceTire());
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }

            $sql =  /** @lang php */ 'SELECT * FROM reclamation_product rp 
                LEFT JOIN reclamation r on rp.ReclamationId = r.ReclamationId
                WHERE rp.costReplaceTire = :costReplaceTire AND 
                r.DealerId = :dealerId AND
                r.typeReclamation = :typeReclamation 
                ORDER BY rp.ReclamationProductId DESC 
                LIMIT :limit 
                OFFSET :offset';
            $stmt = $conn->prepare($sql)
            ;
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('costReplaceTire', $reclamationList->getFilter()->getCostReplaceTire());
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        if ($reclamationList->getFilter()->getUnit()) {
            $sql =  /** @lang php */ 'SELECT * FROM reclamation r 
                LEFT JOIN unitsDealer u on r.unit_id = u.id
                WHERE u.title LIKE :unit AND 
                r.DealerId = :dealerId AND 
                r.typeReclamation = :typeReclamation 
                ORDER BY r.ReclamationId DESC'
            ;
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('unit', $reclamationList->getFilter()->getUnit().'%');
            $stmt->execute();
            $count = $this->getCountForFindByList($stmt->fetchAll());
            $reclamationList->setCount($count);
            if ($reclamationList->getCurrentPage() > 1) {
                $reclamationList->setOffset(PaginationLinks::PAGES_COUNT * ($reclamationList->getCurrentPage() - 1));
            }
            $sql =  /** @lang php */ 'SELECT * FROM reclamation r 
                LEFT JOIN unitsDealer u on r.unit_id = u.id
                WHERE u.title LIKE :unit AND 
                r.DealerId = :dealerId AND 
                r.typeReclamation = :typeReclamation 
                ORDER BY r.ReclamationId DESC
                LIMIT :limit 
                OFFSET :offset'
            ;
            $stmt = $conn->prepare($sql);
            $stmt->bindValue('typeReclamation', $reclamationList->getFilter()->getType());
            $stmt->bindValue('limit', PaginationLinks::PAGES_COUNT, \PDO::PARAM_INT);
            $stmt->bindValue('offset', $reclamationList->getOffset(), \PDO::PARAM_INT);
            $stmt->bindValue('dealerId', $reclamationList->getDealer()->getId());
            $stmt->bindValue('unit', $reclamationList->getFilter()->getUnit().'%');
            $stmt->execute();
            foreach ($stmt->fetchAll() as $reclamation) {
                $reclamations[] = $this->findOneBy(['id' => $reclamation['ReclamationId']]);
            };
            $reclamationList->setReclamations($reclamations);
        }

        return $reclamationList;
    }

    public function findReclamationsForDealer(Dealer $dealer)
    {
        return $this->createQueryBuilder('reclamation')
            ->andWhere('reclamation.dealer = :dealer')
            ->setParameter('dealer', $dealer)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @param $reclamations
     * @return float
     */
    private function getCountForFindByList($reclamations)
    {
        return ceil((count($reclamations))/PaginationLinks::PAGES_COUNT);
    }

    /**
     * @param StateChange $state
     * @return Reclamation[]
     */
    public function findForStatesChangesReclamations(StateChange $state): array
    {
        $builder = $this->createQueryBuilder('r');
        $result = $builder
            ->leftJoin('r.state', 's')
            ->leftJoin('r.stateChanges', 'ch', Expr\Join::WITH, $builder->expr()->eq('ch.stateTo', ':state'))
            ->where($builder->expr()->eq('r.state', ':state'))
            ->andWhere($builder->expr()->lte('ch.datetime',':date'))
            ->setParameter('state', $state->getState())
            ->setParameter('date', (new \DateTime())->sub($state->getChangeDelay()))
            ->getQuery()
            ->getResult()
        ;
        return $result;
    }

}