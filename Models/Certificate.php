<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Certificate
 *
 * @ORM\Table(name="certificate")
 * @ORM\Entity(repositoryClass="App\Repository\CertificateRepository")
 */
class Certificate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="UserId")
     */
    private $user;

    /**
     * @var TyreCai[]
     *
     * @ORM\ManyToMany(targetEntity="TyreCai")
     * @ORM\JoinTable(name="certificate_cais",
     *      joinColumns={@ORM\JoinColumn(name="certificate_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="cai_id", referencedColumnName="id")}
     * )
     */
    private $cais;

    /**
     * @var UploadFile[]
     *
     * @ORM\ManyToMany(targetEntity="UploadFile", cascade={"persist"})
     * @ORM\JoinTable(name="certificate_files",
     *      joinColumns={@ORM\JoinColumn(name="certificate_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true)}
     * )
     */
    private $files;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=255)
     */
    private $number;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateTo", type="datetime", nullable=true)
     */
    private $dateTo;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return TyreCai[]|Collection
     */
    public function getCais(): Collection
    {
        return $this->cais;
    }

    /**
     * @param TyreCai[]|Collection $cais
     */
    public function setCais(Collection $cais)
    {
        $this->cais = $cais;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber(string $number)
    {
        $this->number = $number;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return UploadFile[]|Collection
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    /**
     * @param UploadFile[]|Collection $files
     */
    public function setFiles(Collection $files)
    {
        $this->files = $files;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * @param $dateTo
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;
    }
}