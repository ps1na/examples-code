<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="tyre_data")
 * @ORM\Entity
 */
class TyreData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", length=11, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TyreMark")
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
     * @var TyreMark|null
     */
    private $mark;

    /**
     * @ORM\ManyToOne(targetEntity="TyreSize")
     * @ORM\JoinColumn(name="size_id", referencedColumnName="id")
     * @var TyreSize|null
     */
    private $size;

    /**
     *
     * @ORM\ManyToOne(targetEntity="TyreIndex")
     * @ORM\JoinColumn(name="index_id", referencedColumnName="id")
     * @var TyreIndex|null
     */
    private $index;

    /**
     * @ORM\ManyToOne(targetEntity="TyreModel")
     * @ORM\JoinColumn(name="model_id", referencedColumnName="id")
     * @var TyreModel|null
     */
    private $model;

    /**
     * @ORM\ManyToOne(targetEntity="TyreCai")
     * @ORM\JoinColumn(name="cai_id", referencedColumnName="id")
     * @var TyreCai|null
     */
    private $cai;

    /**
     * @ORM\Column(name="brand_title", type="string", length=255, nullable=true)
     * @var string
     */
    private $brandTitle;

    /**
     * @ORM\Column(name="size_title", type="string", length=255, nullable=true)
     * @var string
     */
    private $sizeTitle;

    /**
     * @ORM\Column(name="index_title", type="string", length=255, nullable=true)
     * @var string
     */
    private $indexTitle;

    /**
     * @ORM\Column(name="model_title", type="string", length=255, nullable=true)
     * @var string
     */
    private $modelTitle;

    /**
     * @ORM\Column(name="cai_title", type="string", length=255, nullable=true)
     * @var string
     */
    private $caiTitle;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return TyreMark|null
     */
    public function getMark(): ?TyreMark
    {
        return $this->mark;
    }

    /**
     * @return TyreSize|null
     */
    public function getSize(): ?TyreSize
    {
        return $this->size;
    }

    /**
     * @return TyreIndex|null
     */
    public function getIndex(): ?TyreIndex
    {
        return $this->index;
    }

    /**
     * @return TyreModel|null
     */
    public function getModel(): ?TyreModel
    {
        return $this->model;
    }

    /**
     * @return TyreCai|null
     */
    public function getCai(): ?TyreCai
    {
        return $this->cai;
    }


    /**
     * @return string
     */
    public function getBrandTitle(): string
    {
        return $this->brandTitle;
    }

    /**
     * @return string
     */
    public function getSizeTitle(): string
    {
        return $this->sizeTitle;
    }

    /**
     * @return string
     */
    public function getIndexTitle(): string
    {
        return $this->indexTitle;
    }

    /**
     * @return string
     */
    public function getModelTitle(): string
    {
        return $this->modelTitle;
    }

    /**
     * @return string
     */
    public function getCaiTitle(): string
    {
        return $this->caiTitle;
    }

}