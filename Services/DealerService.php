<?php

namespace App\Service\Admin;

use App\Dto\DealerDto;
use App\Entity\Dealer;
use App\FileUploader;
use App\Repository\DealerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DealerService
{
    /**
     *
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    /**
     * @var DealerRepository $dealerRepository
     */
    private $dealerRepository;

    /**
     * @var FileUploader
     */
    private $fileUploader;

    /**
     * DealerService constructor.
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     * @param DealerRepository $dealerRepository
     * @param FileUploader $fileUploader
     */
    public function __construct(
        EntityManagerInterface $em,
        UserPasswordEncoderInterface  $encoder,
        DealerRepository $dealerRepository,
        FileUploader $fileUploader
    )
    {
        $this->em = $em;
        $this->encoder = $encoder;
        $this->dealerRepository = $dealerRepository;
        $this->fileUploader = $fileUploader;
    }

    /**
     * @param DealerDto $dto
     * @throws \Exception
     */
    public function create(DealerDto $dto): void
    {
        $this->em->beginTransaction();
        try {
            if(!$this->validLoginName($dto->getLogin())){
                $dealer = new Dealer();
                $dealer->setDealerNew(
                    $dto->getLogin(),
                    $dto->getPostCode(),
                    $dto->getEmail(),
                    $dto->getPhone(),
                    $dto->getName(),
                    $dto->getFirstName(),
                    $dto->getSurname(),
                    $dto->getOrderCode(),
                    $dto->getFactoredCode(),
                    $dto->getTitle(),
                    $dto->getAddress(),
                    $dto->getOpening(),
                    $dto->getRoles(),
                    $dto->getLocked(),
                    $dto->getFt(),
                    $dto->getCountry(),
                    $dto->getEmailregmangerSales(),
                    $dto->getCooperationPrograms()
                );
                $dealer->setSalt(md5(uniqid(rand(), true)));
                $dealer->setPassword($this->encoder->encodePassword($dealer,$dto->getPassword()));
                $this->em->persist($dealer);
                $this->em->flush();
                $this->em->commit();
            }
            else {
                throw new \Exception('Дилер с таким логином уже существует, и поэтому операция не может быть выполнена');
            }

        } catch (\Exception $exception) {
            $this->em->rollback();
            throw new \Exception($exception->getMessage().'');

        }
    }

    public function validLoginName($name){

       if($this->dealerRepository->countByCriteria(['login'=>$name],true))
       {
           return true;
       }
       return false;

    }

    /**
     * @param Dealer $dealer
     * @param DealerDto $dto
     * @throws \Exception
     */
    public function update(Dealer $dealer, DealerDto $dto): void
    {
        $this->em->beginTransaction();
        try {

          $password = $this->encoder->encodePassword($dealer,$dto->getPassword());


            if($dto->getPassword()) {
              $dealer->setPassword($password);
            }
             if($dto->getLogin()==$dealer->getLogin())
             {

             }elseif (!$this->validLoginName($dto->getLogin())){

                 $dealer->setLogin($dto->getLogin());

             }else{
                 throw new \Exception('Дилер с таким логином уже существует, и поэтому операция не может быть выполнена');
             }
                 $dealer->setEmail($dto->getEmail());
                 $dealer->setName($dto->getTitle());
                 if($dto->getCountry()) {
                     $dealer->setCountry($dto->getCountry());
                 }
                 $dealer->setAddress($dto->getAddress());
                 $dealer->setHoursWork($dto->getHoursWork());
                 $dealer->setFirstName($dto->getFirstName());
                 $dealer->setLastName($dto->getSurname());
                 $dealer->setMiddleName($dto->getName());
                 $dealer->setJcodeDelivery($dto->getOrderCode());
                 $dealer->setJcodeFacture($dto->getFactoredCode());
                 $dealer->setPhone($dto->getPhone());
                 $dealer->setPostalCode($dto->getPostCode());
                 $dealer->setHoursWork($dto->getOpening());
                 $dealer->setRoles($dto->getRoles());
                 $dealer->setLocked($dto->getLocked());
                 $dealer->setFt($dto->getFt());
                 $dealer->setEmailregmangerSales($dto->getEmailregmangerSales());
                 $dealer->setCooperationPrograms($dto->getCooperationPrograms());

            $this->em->persist($dealer);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $exception) {
            $this->em->rollback();
            throw new \Exception($exception->getMessage().'');
        }
    }

    /**
     * @param Dealer $dealer
     * @throws \Exception
     */
    public function delete(Dealer $dealer): void
    {
        $this->em->beginTransaction();
        try {

            $dealer->setLogin('HOT LINES');
            $dealer->setPassword('');
            $dealer->setEmail('Удаление на основании 152ФЗ');
            $dealer ->setName('Удаление на основании 152ФЗ');
            $dealer ->setAddress('Удаление на основании 152ФЗ');
            $dealer  ->setHoursWork('Удаление на основании 152ФЗ');
            $dealer ->setFirstName('Удаление на основании 152ФЗ');
            $dealer ->setLastName('Удаление на основании 152ФЗ');
            $dealer ->setMiddleName('Удаление на основании 152ФЗ');
            $dealer ->setJcodeDelivery('Удаление на основании 152ФЗ');
            $dealer ->setJcodeFacture('Удаление на основании 152ФЗ');
            $dealer ->setPhone('Удаление на основании 152ФЗ');
            $dealer ->setPostalCode('Удаление на основании 152ФЗ');
            $dealer  ->setHoursWork('Удаление на основании 152ФЗ');
            $dealer->setIsdeleted(1);

            $this->em->persist($dealer);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $exception) {
            $this->em->rollback();
            throw new \Exception($exception->getMessage());
        }
    }

}