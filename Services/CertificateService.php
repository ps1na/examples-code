<?php

namespace App\Service;


use App\Dto\CertificateDto;
use App\Dto\CertificateFileDto;
use App\Dto\TyreCaiDto;
use App\Entity\Certificate;
use App\Entity\TyreCai;
use App\Entity\UploadFile;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CertificateService
 * @package App\Service
 */
class CertificateService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * CertificateService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param CertificateDto $certificateDto
     * @throws \Exception
     */
    public function create(CertificateDto $certificateDto)
    {
        $this->em->beginTransaction();
        $certificate = new Certificate();
        try {
            $certificate->setFiles($certificateDto->getFilesDto());
            $certificate->setNumber($certificateDto->getNumber());
            $certificate->setDescription($certificateDto->getDescription());
            $certificate->setCais($certificateDto->getCais());
            $certificate->setDateTo($certificateDto->getDateTo());
            $this->em->persist($certificate);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $exception) {
            $this->em->rollback();
            throw new \Exception('Ошибка при создании сертификата.');
        }
    }

    /**
     * @param Certificate $certificate
     * @param CertificateDto $certificateDto
     * @throws \Exception
     */
    public function update(Certificate $certificate, CertificateDto $certificateDto)
    {
        $this->em->beginTransaction();
        try {
            $certificate->setFiles($certificateDto->getFilesDto());
            $certificate->setNumber($certificateDto->getNumber());
            $certificate->setDescription($certificateDto->getDescription());
            $certificate->setCais($certificateDto->getCais());
            $certificate->setDateTo($certificateDto->getDateTo());
            $this->em->persist($certificate);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $exception) {
            $this->em->rollback();
            throw new \Exception('Ошибка при редактировании сертификата.');
        }
    }

    /**
     * @param Certificate $certificate
     * @throws \Exception
     */
    public function delete(Certificate $certificate)
    {
        $this->em->beginTransaction();
        try {
            //TODO: set is deleted
            $this->em->remove($certificate);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $exception) {
            $this->em->rollback();
            throw new \Exception('Неизвестная ошибка.');
        }
    }
}