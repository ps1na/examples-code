<?php

namespace App\Service;


use App\Entity\Dealer;
use App\Service\Notification\Email\EmailNotification;
use App\Service\Notification\Notifier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RestorePasswordService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var Notifier
     */
    private $notifier;

    /**
     * RestorePasswordService constructor.
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     * @param Notifier $notifier
     */
    public function __construct(
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $encoder,
        Notifier $notifier
    ) {
        $this->em = $em;
        $this->encoder = $encoder;
        $this->notifier = $notifier;
    }

    /**
     * @param Dealer[] $dealers[]
     * @throws \Exception
     */
    public function update($dealers): void
    {
        $randomPasswords = [];
        foreach ($dealers as $dealer) {
            try {
                $this->em->beginTransaction();
                $randomPassword = bin2hex(random_bytes(6));
                $password = $this->encoder->encodePassword($dealer,$randomPassword);
                $dealer->setPassword($password);
                $this->em->persist($dealer);
                $this->em->flush();
                $this->em->commit();
                $randomPasswords[] = $randomPassword;
            }
            catch (\Exception $exception) {
                $this->em->rollback();
                throw new \Exception($exception->getMessage());
            }
        }

        try {
            if (count($dealers) > 1) {
                $message = $this->getTemplateMails($dealers, $randomPasswords);
                foreach ($dealers as $dealer) {
                    $this->notifyDealer($message, $dealer);
                }
            } else {
                $dealer = reset($dealers);
                $message = $this->getTemplateMail($dealer, reset($randomPasswords));
                $this->notifyDealer($message, $dealer);
            }
        } catch (\Exception $exception) {
//            FIXME
        }
    }

    /**
     * @param string $message
     * @param Dealer $dealer
     */
    private function notifyDealer(string $message, Dealer $dealer)
    {
        $emailNotification = new EmailNotification($message, '');
        $this->notifier->notify($emailNotification, $dealer);
    }

    /**
     * @param Dealer $dealer
     * @param string $randomPassword
     * @return string
     */
    private function getTemplateMail(Dealer $dealer, string $randomPassword)
    {
        $message = 'Вы запросили восстановление пароля для имени '.$dealer->getLogin().', чтобы войти в XXX. Вот этот пароль: '.$randomPassword;
        return $message;
    }

    /**
     * @param $dealers
     * @param array $randomPasswords
     * @return string
     */
    private function getTemplateMails($dealers, array $randomPasswords)
    {
        $mail = reset($dealers)->getEmail();
        $message = 'Вы запросили восстановление пароля для адреса электронной почты '.$mail.', чтобы войти в XXX. На данный адрес зарегистрированы следующие имена / пароли для входа:<br/><br/>';
        $key = 0;
        /**
         * @var Dealer[] $dealers
         */
        foreach ($dealers as $dealer) {
            $message .= '<br/>';
            $message .= $dealer->getLogin().' / '.$randomPasswords[$key];
            ++$key;
        }

        return $message;
    }
}