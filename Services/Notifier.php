<?php

namespace App\Service\Notification;

class Notifier
{
    /**
     * @var HandlerInterface[]
     */
    private $handlers;

    public function __construct(array $handlers = []) {
        foreach ($handlers as $handler) {
            if (!$handler instanceof HandlerInterface) {
                throw new \InvalidArgumentException(sprintf('%s should be instance of ' . HandlerInterface::class, get_class($handler)));
            }
            $this->handlers[] = $handler;
        }
    }

    /**
     * @param NotificationInterface $notification
     * @param RecipientInterface $recipient
     */
    public final function notify(NotificationInterface $notification, RecipientInterface $recipient): void
    {
        foreach ($this->handlers as $handler) {
            try {
                if ($handler->shouldNotify($notification, $recipient)) {
                    $handler->notify($notification, $recipient);
                }
            } catch (\Exception $exception) {

            }
        }
    }

    /**
     * @param NotificationInterface $notification
     * @param RecipientInterface[] $recipients
     */
    public final function notifyAll(NotificationInterface $notification, array $recipients): void
    {
        foreach ($recipients as $recipient) {
            $this->notify($notification, $recipient);
        }
    }


}