<?php

namespace App\Controller\Api;

use App\Entity\Reclamation;
use App\Entity\TyreSize;
use App\Enum\ReclamationGuaranteeSolution;
use App\Enum\ReclamationState;
use App\Enum\ReclamationType;
use App\Enum\TransportType;
use App\Repository\CaiRepository;
use App\Repository\MarkTyreRepository;
use App\Repository\ReclamationRepository;
use App\Repository\TyreIndexRepository;
use App\Repository\TyreModelRepository;
use App\Repository\TyreSizeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\JsonSerializableNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Workflow\Registry;

/**
 * @Route("/api/reclamations", name="api_reclamation_")
 */
class ReclamationController extends Controller
{
    /**
     * @var ReclamationRepository
     */
    private $reclamationRepository;

    /**
     * @var MarkTyreRepository
     */
    private $tyreMarkRepository;

    /**
     * @var TyreSizeRepository;
     */
    private $tyreSizeRepository;

    /**
     * @var TyreIndexRepository
     */
    private $tyreIndexRepository;

    /**
     * @var TyreModelRepository
     */
    private $tyreModelRepository;

    /**
     * @var CaiRepository
     */
    private $tyreCaiRepository;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Registry
     */
    private $workflows;


    /**
     * ReclamationController constructor.
     * @param ReclamationRepository $repository
     * @param TyreSizeRepository $tyreSizeRepository
     * @param TyreIndexRepository $tyreIndexRepository
     * @param TyreModelRepository $tyreModelRepository
     * @param CaiRepository $tyreCaiRepository
     * @param MarkTyreRepository $markTyreRepository
     * @param EntityManagerInterface $em
     * @param Registry $registry
     */
    public function __construct(
        ReclamationRepository $repository,
        TyreSizeRepository $tyreSizeRepository,
        TyreIndexRepository $tyreIndexRepository,
        TyreModelRepository $tyreModelRepository,
        CaiRepository $tyreCaiRepository,
        MarkTyreRepository $markTyreRepository,
        EntityManagerInterface $em,
        Registry $registry
    ) {
        $this->reclamationRepository = $repository;
        $this->tyreSizeRepository = $tyreSizeRepository;
        $this->serializer = new Serializer(array(new JsonSerializableNormalizer(), new DateTimeNormalizer()), array(new JsonEncoder()));
        $this->tyreIndexRepository = $tyreIndexRepository;
        $this->tyreModelRepository = $tyreModelRepository;
        $this->tyreCaiRepository = $tyreCaiRepository;
        $this->tyreMarkRepository = $markTyreRepository;
        $this->em = $em;
        $this->workflows = $registry;
    }

    /**
     * @Route("/{id}",
     *     name="show",
     *     methods="GET",
     *     requirements={"id"="\d+"},
     * )
     * @param Reclamation $reclamation
     * @return JSONResponse
     */
    public function getAction(Reclamation $reclamation): JsonResponse
    {
        $json = $this->serializer->serialize($reclamation, 'json');
        return new JsonResponse($json, 200, array(), true);
    }

    /**
     * @Route("/list",
     *     name="list",
     *     methods="GET"
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function getListAction(Request $request): JsonResponse
    {
        $ids = $request->get("ids", "");
        $reclamationIds = explode(",", $ids);
        $reclamations = $this->reclamationRepository->findByCriteria(['id' => $reclamationIds], null, null, null, true);
        $json = $this->serializer->serialize($reclamations, 'json');
        return new JsonResponse($json, 200, array(), true);
    }

    /**
     * @Route("/page/{page}",
     *     name="page",
     *     methods="GET",
     *     requirements={"page"="\d+"},
     * )
     * @param int $page
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getPageAction(int $page, Request $request): JsonResponse
    {
        $filter = json_decode($request->get('filter', ''), $assoc = true);
        $reclamations = $this->build($filter)
            ->setMaxResults(10)
            ->setFirstResult(($page - 1) * 10)
            ->orderBy('r.id', 'DESC')
            ->getQuery()
            ->getResult();

        $json = $this->serializer->serialize($reclamations, 'json');
        return new JsonResponse($json, 200, array(), true);
    }

    /**
     * TODO: burn
     * @param $filter
     * @return \Doctrine\ORM\QueryBuilder
     * @throws \Exception
     */
    private function build($filter)
    {
        $queryBuilder = $this->reclamationRepository->createQueryBuilder('r');
        $joins = [];
        foreach ($filter as $key => $value) {
            if ($key === 'require_transportation') {
                $joins[] = 'reclamationProduct';
                $joins[] = 'state';
                $queryBuilder->where(
                    $queryBuilder->expr()->andX(
                        $queryBuilder->expr()->eq('REGEXP(r.number, \'^FT[0-9]*$\')', ':false'),
                        $queryBuilder->expr()->orX(
                            // bf_sw_replace
                            $queryBuilder->expr()->andX(
                                $queryBuilder->expr()->orX(
                                    $queryBuilder->expr()->eq('r.typeReclamation', ':type_bf'),
                                    $queryBuilder->expr()->eq('r.typeReclamation', ':type_sw')
                                ),
                                $queryBuilder->expr()->eq('reclamationProduct.solution', ':solution_replace'),
                                $queryBuilder->expr()->eq('state.id', ':state_open')
                            ),
                            // pl_test_ew
                            $queryBuilder->expr()->andX(
                                $queryBuilder->expr()->orX(
                                    $queryBuilder->expr()->eq('r.typeReclamation', ':type_ew'),
                                    $queryBuilder->expr()->andX(
                                        $queryBuilder->expr()->eq('r.typeReclamation', ':type_normal'),
                                        $queryBuilder->expr()->eq('r.transportType', ':transport_pl')
                                    )
                                ),
                                $queryBuilder->expr()->eq('state.id', ':state_open')
                            ),
                            // tc_moto
                            $queryBuilder->expr()->andX(
                                $queryBuilder->expr()->eq('r.typeReclamation', ':type_normal'),
                                $queryBuilder->expr()->orX(
                                    $queryBuilder->expr()->eq('r.transportType', ':transport_tc'),
                                    $queryBuilder->expr()->eq('r.transportType', ':transport_moto')
                                ),
                                $queryBuilder->expr()->eq('state.id', ':state_correct_docs')
                            )
                        )
                    )
                );
                $queryBuilder->setParameters(array(
                    'false' => false,
                    'type_bf' => ReclamationType::BF,
                    'type_sw' => ReclamationType::SW,
                    'type_ew' => ReclamationType::EW,
                    'type_normal' => ReclamationType::NORMAL,
                    'transport_tc' => TransportType::TC,
                    'transport_moto' => TransportType::MOTO,
                    'transport_pl' => TransportType::PL,
                    'solution_replace' => ReclamationGuaranteeSolution::REPLACE,
                    'state_open' => ReclamationState::OPEN,
                    'state_correct_docs' => ReclamationState::CORRECT_DOCS,
                ));
                continue;
            }
            $fieldName = $key;
            $field = "r.".$key;
            $searchString = $value;
            $parameter = $fieldName;
            if (is_array($value)) {
                $relation = $key;
                $joins[] = $relation;
                $fieldName = array_keys($value)[0];
                $field = "$relation.$fieldName";
                $parameter = $relation."_".$fieldName;
                $searchString = $value[$fieldName];
            } else {
                $parts = explode('.', $key);
                if (count($parts) === 2) {
                    list($relation, $fieldName) = $parts;
                    $field = "$relation.$fieldName";
                    $parameter = $relation."_".$fieldName;
                    $joins[] = $relation;
                    $searchString = $value;
                } else if (count($parts) > 1) {
                    throw new \Exception("Unknown field $key on reclamation");
                }
            }
            if (preg_match('/^\%.*\%$/', $searchString)) {
                $queryBuilder->andWhere("$field LIKE :$parameter");
            } else {
                $queryBuilder->andWhere("$field = :$parameter");
            }
            $queryBuilder
                ->setParameter($parameter, $searchString);
        }
        foreach (array_unique($joins) as $relation) {
            $queryBuilder
                ->leftJoin("r.$relation", $relation);
        }
        return $queryBuilder;
    }

    /**
     * @Route("/total", name="total", methods="GET")
     * @param Request $request
     * @return JsonResponse
     */
    public function totalAction(Request $request): JsonResponse
    {
        $filter = json_decode($request->get('filter', ''), $assoc = true);
        try {
            $total = $this->build($filter)
                ->select('count(r.id)')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (\Exception $exception) {
            $total = 0;
        }
        return new JsonResponse($total);
    }

    // FIXME: PLEASE USE DEFAULT SYMFONY BUILDER
    /**
     * @Route("/getSizeIndexForReclamation", name="getSizeIndexForReclamation")
     * @param Request $request
     * @return string
     */
    public function getSizeIndexForReclamation(Request $request)
    {
        $markId = $request->get('mark_tyre_id');
        $sizeId = $request->get('size_tyre_id');
        $indexId = $request->get('index_tyre_id');
        $modelId = $request->get('model_tyre_id');
        $typeSelect = $request->get('type');
        $conn = $this->getDoctrine()->getConnection();

        if ($typeSelect === 'size') {

            $sql = 'SELECT `size_id` FROM (SELECT DISTINCT(`size_id`), `size_title` FROM `tyre_data` WHERE `brand_id` = :markId) TD ORDER BY CONVERT(SUBSTRING_INDEX(`size_title`, \'/\', 1), DECIMAL(10,2)), CONVERT(SUBSTRING_INDEX(SUBSTRING_INDEX(`size_title`, \'/R\', 1), \'/\', -1), DECIMAL), CONVERT(MID(MID(`size_title`, LOCATE(\'/R\', `size_title`)), 3), DECIMAL(10,1))';

            $stmt = $conn->prepare($sql);
            $stmt->execute(['markId' => $markId]);

            $result = $stmt->fetchAll();
            $size = [];
            foreach ($result as $item) {
                /**
                 * @var $tyreSizes TyreSize
                 */
                $tyreSizes = $this->tyreSizeRepository->findBy(['id' => $item['size_id']]);
                $tyreSize = reset($tyreSizes);
                $tyreSize->setTitle(ltrim($tyreSize->getTitle(), '0'));
                $size[] = $tyreSizes;
            }
            $string = $this->renderView('front/api/size.html.twig', [
                'size' => $size
            ]);

            return new Response($string);
        }

        if ($typeSelect === 'index') {

            $sql = 'SELECT DISTINCT(index_id), index_title FROM tyre_data WHERE brand_id = :markId AND size_id = :sizeId ORDER BY index_title';
            $stmt = $conn->prepare($sql);
            $stmt->execute([
                'markId' => $markId,
                'sizeId' => $sizeId
            ]);

            $result = $stmt->fetchAll();
            $index = [];
            foreach ($result as $item) {
                $index[] = $this->tyreIndexRepository->findBy(['id' => $item['index_id']]);
            }
            $string = $this->renderView('front/api/index.html.twig', [
                'index' => $index
            ]);

            return new Response($string);
        }

        if ($typeSelect === 'model') {
            $sql = 'SELECT DISTINCT(model_id), model_title FROM tyre_data WHERE brand_id = :markId AND size_id = :sizeId AND index_id = :indexId ORDER BY model_title';
            $stmt = $conn->prepare($sql);
            $stmt->execute([
                'markId' => $markId,
                'sizeId' => $sizeId,
                'indexId' => $indexId
            ]);

            $result = $stmt->fetchAll();
            $model = [];
            foreach ($result as $item) {
                $model[] = $this->tyreModelRepository->findBy(['id' => $item['model_id']]);
            }
            $string = $this->renderView('front/api/model.html.twig', [
                'model' => $model
            ]);

            return new Response($string);
        }

        if ($typeSelect === 'cai') {
            $sql = 'SELECT DISTINCT(cai_id) FROM tyre_data WHERE brand_id = :markId AND size_id = :sizeId AND index_id = :indexId AND model_id = :modelId';
            $stmt = $conn->prepare($sql);
            $stmt->execute([
                'markId' => $markId,
                'sizeId' => $sizeId,
                'indexId' => $indexId,
                'modelId' => $modelId
            ]);

            $result = $stmt->fetchAll();
            $cai = [];
            foreach ($result as $item) {
                $cai[] = $this->tyreCaiRepository->findBy(['id' => $item['cai_id']]);
            }
            $string = $this->renderView('front/api/cai.html.twig', [
                'cai' => $cai
            ]);

            return new Response($string);
        }

    }

    /**
     * @Route("/getCai", name="getCai")
     * @param Request $request
     * @return string
     */
    public function getOnlyCai(Request $request)
    {
        $caiTitle = $request->get('tyre_cai_title');
        $sql = 'SELECT * FROM tyre_data WHERE cai_title = :cai_title';

        $conn = $this->getDoctrine()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute([
            'cai_title' => $caiTitle
        ]);
        $result = $stmt->fetchAll();
        if (!$result) {
            return new JsonResponse();
        }

        $tempResult = [
            'mark' => [],
            'size' => [],
            'index' => [],
            'model' => [],
            'cai' => [],
        ];
        foreach ($result as $item) {
            $tempResult['mark'] = $this->tyreMarkRepository->findBy(['title' => $item['brand_title']]);
            $tempResult['size'] = $this->tyreSizeRepository->findBy(['title' => $item['size_title']]);
            $tempResult['index'] = $this->tyreIndexRepository->findBy(['title' => $item['index_title']]);
            $tempResult['model'] = $this->tyreModelRepository->findBy(['title' => $item['model_title']]);
            $tempResult['cai'] = $this->tyreCaiRepository->findBy(['title' => $item['cai_title']]);
        }
        $stringMark = $this->renderView('front/api/item.html.twig', [
            'item' => reset($tempResult['mark'])
        ]);
        $stringSize = $this->renderView('front/api/item.html.twig', [
            'item' => reset($tempResult['size'])
        ]);
        $stringIndex = $this->renderView('front/api/item.html.twig', [
            'item' => reset($tempResult['index'])
        ]);
        $stringModel = $this->renderView('front/api/item.html.twig', [
            'item' => reset($tempResult['model'])
        ]);
        $stringCai = $this->renderView('front/api/item.html.twig', [
            'item' => reset($tempResult['cai'])
        ]);

        return new JsonResponse([
            'mark' => $stringMark,
            'size' => $stringSize,
            'index' => $stringIndex,
            'model' => $stringModel,
            'cai' => $stringCai
        ]);

    }
}