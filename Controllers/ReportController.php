<?php

namespace App\Controller\Admin;

use App\Entity\Dealer;
use App\Entity\ReportStatus;
use App\Entity\State;
use App\Repository\DealerRepository;
use App\Repository\ReclamationRepository;
use App\Repository\StateRepository;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class ReportController
 * @Route("/admin/report", name="admin_report_")
 * @package App\Controller\Admin
 */
class ReportController extends AbstractController
{
    /**
     * @var ReclamationRepository
     */
    private $reclamationRepository;

    /**
     * @var StateRepository
     */
    private $stateRepository;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var DealerRepository
     */
    private $dealerRepository;

    /**
     * ReportController constructor.
     * @param ReclamationRepository $reclamationRepository
     * @param StateRepository $stateRepository
     * @param SessionInterface $session
     * @param DealerRepository $dealerRepository
     */
    public function __construct(
        ReclamationRepository $reclamationRepository,
        StateRepository $stateRepository,
        SessionInterface $session,
        DealerRepository $dealerRepository
    )
    {
        $this->reclamationRepository = $reclamationRepository;
        $this->stateRepository = $stateRepository;
        $this->session = $session;
        $this->dealerRepository = $dealerRepository;
    }

    /**
     * @Route("/", name="index")
     * @Template
     */
    public function indexAction(){}

    /**
     * @Route("/status-report", name="status-report")
     * @return Response
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function statusReportAction(): Response
    {
        $allStates = $this->stateRepository->findAll();
        $resultStates = [];
        /**
         * @var State $state
         */
        foreach ($allStates as $state) {
            $r = new ReportStatus();
            $r->setTitle($state->getName());
            $r->setIdState($state->getId());
            /**
             * @var ReportStatus[] $resultStates
             */
            $resultStates[] = $r;
        }

        $reportStatus = $this->reclamationRepository->findReclamationForStatusReport($resultStates);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValueByColumnAndRow(1,1,'Статус');
        $sheet->setCellValueByColumnAndRow(2,1,'TC');
        $sheet->setCellValueByColumnAndRow(3,1,'PL');
        $sheet->setCellValueByColumnAndRow(4,1,'GC');
        $row = 2;
        foreach ($reportStatus as $key => $item) {
            $sheet->setCellValueByColumnAndRow(1,$row,$item->getTitle());
            $sheet->setCellValueByColumnAndRow(2,$row,$item->getCountTC());
            $sheet->setCellValueByColumnAndRow(3,$row,$item->getCountPL());
            $sheet->setCellValueByColumnAndRow(4,$row,$item->getCountGC());
            $row++;
        }
        $writer = new Xlsx($spreadsheet);
        $nameFile = './reports/Report_Status.xlsx';
        $writer->save($nameFile);

        return $this->redirect('/'.$nameFile);
    }

    /**
     * @Route("/commercial-pivot-report", name="commercial-pivot-report")
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function commercialPivotReport(){
        $dealers = $this->dealerRepository->findBy([],
            [
                'name' => 'ASC'
            ]);
        /** @var Dealer $dealer */
        foreach ($dealers as $dealer) {
            $reclamations = $this->reclamationRepository->findReclamationsForDealer($dealer);
            $dealer->setReclamations($reclamations);
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet = $this->setHeadersForPivot($sheet);

        $row = 2;
        $column = 1;
        foreach ($dealers as $dealer) {
//            Дилер
            $sheet->setCellValueByColumnAndRow($column,$row,$dealer->getName());
            $column++;
            $sheet->setCellValueByColumnAndRow($column,$row,$dealer->getJcodeFacture());
            $column++;
            $sheet->setCellValueByColumnAndRow($column,$row,$dealer->getJcodeDelivery());
            $column++;
            $sheet->setCellValueByColumnAndRow($column,$row,$dealer->getLastName());
            $column++;
            $sheet->setCellValueByColumnAndRow($column,$row,$dealer->getFirstName());
            $column++;
            $sheet->setCellValueByColumnAndRow($column,$row,$dealer->getMiddleName());
            $column++;
            $sheet->setCellValueByColumnAndRow($column,$row,'Область');
            $column++;
            $sheet->setCellValueByColumnAndRow($column,$row,$dealer->getAddress());
            $column++;
            $sheet->setCellValueByColumnAndRow($column,$row,$dealer->getEmail());
            $column++;
            $sheet->setCellValueByColumnAndRow($column,$row,$dealer->getPhone());
            $column++;
//            ТКП
            $sheet->setCellValueByColumnAndRow($column,$row,'ТКП Фамилия');
            $column++;
            $sheet->setCellValueByColumnAndRow($column,$row,'ТКП Имя');
            $column++;
            foreach ($dealer->getReclamations() as $reclamation) {
                $firstColumn = $column;
//                Пользователь
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getTyreUser() ? $reclamation->getTyreUser()->getLastName() : '');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getTyreUser() ? $reclamation->getTyreUser()->getFirstName() : '');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getTyreUser() ? $reclamation->getTyreUser()->getMiddleName() : '');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getTyreUser() ? $reclamation->getTyreUser()->getEnterprise() : '');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getTyreUser() ? 'Область' : '');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getTyreUser() ? $reclamation->getTyreUser()->getAddress() : '');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getTyreUser() ? $reclamation->getTyreUser()->getPhone() : '');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getTyreUser() ? $reclamation->getTyreUser()->getEmail() : '');
                $firstColumn++;
//                Рекламация
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getNumber());
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getState() ? $reclamation->getState()->getName() : '');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getProcessingTime()->format('Y.m.d'));
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,'Необходимость вывоза на склад');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,'Дата доставки на склад');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,'Дата доставки в лабороторию QTB');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getCompensation());
                $firstColumn++;
//                Рекламационный продукт
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,'Дата готовности шины к вывозу');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getReclamationProduct() ? $reclamation->getReclamationProduct()->getReasonReclamation() : '');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getReclamationProduct() ? $reclamation->getReclamationProduct()->getTyreSize() ? $reclamation->getReclamationProduct()->getTyreSize()->getTitle() : '' : '');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getReclamationProduct() ? $reclamation->getReclamationProduct()->getSerialNumber() : '');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getReclamationProduct() ? $reclamation->getReclamationProduct()->getTyreModel() ? $reclamation->getReclamationProduct()->getTyreModel()->getTitle() : '' : '');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getReclamationProduct() ? $reclamation->getReclamationProduct()->getDot() : '');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getReclamationProduct() ? $reclamation->getReclamationProduct()->getIndexLoadOrSpeed() ? $reclamation->getReclamationProduct()->getIndexLoadOrSpeed()->getTitle() : '' : '');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getReclamationProduct() ? $reclamation->getReclamationProduct()->getCai() ? $reclamation->getReclamationProduct()->getCai()->getTitle() : '' : '');
                $firstColumn++;
                $sheet->setCellValueByColumnAndRow($firstColumn,$row,$reclamation->getCommentReclamation());
                $firstColumn++;

                $row++;
            }
            $column = 1;
        }

        $writer = new Xlsx($spreadsheet);
        $tempFile = tempnam(sys_get_temp_dir(), 'report');
        $writer->save($tempFile);

        $response = new BinaryFileResponse($tempFile);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'Отчёт_для_коммерции_pivot.xlsx'
        );

        return $response;
    }

    /**
     * @param Worksheet $sheet
     * @return Worksheet
     */
    private function setHeadersForPivot(Worksheet $sheet)
    {
        $column = 1;
        $row = 1;
        foreach ($this->getAllField() as $key => $value) {
            $sheet->setCellValueByColumnAndRow($column,$row,$value);
            $column++;
        }

        return $sheet;
    }

    /**
     * @return array
     */
    private function getAllField()
    {
        // Порядок влияет на порядок зоголовков: изменяя здесь, не забудь поправить commercialPivotReport()
        return [
            'Дилер Название',
            'Дилер Фактурный код',
            'Дилер Доставочный код',
            'Дилер Фамилия',
            'Дилер Имя',
            'Дилер Отчество',
            'Дилер Область',
            'Дилер Адрес',
            'Дилер E-mail',
            'Дилер Телефон',

            'ТКП Фамилия',
            'ТКП Имя',

            'Пользователь Фамилия',
            'Пользователь Имя',
            'Пользователь Отчество',
            'Пользователь Организация',
            'Пользователь Область',
            'Пользователь Адрес',
            'Пользователь Телефон',
            'Пользователь E-mail',

            'Рекламация Номер рекламации',
            'Рекламация Статус',
            'Рекламация Дата обращения пользователя',
            'Рекламация Необходимость вывоза шины',
            'Рекламация Дата доставки на склад',
            'Рекламация Дата доставки в лабороторию QTB',
            'Рекламация Компенсация',

            'Рекламационны_продукт Дата готовности шины к вывозу',
            'Рекламационны_продукт Причина',
            'Рекламационны_продукт Размерность',
            'Рекламационны_продукт Серийный номер',
            'Рекламационны_продукт Модель',
            'Рекламационны_продукт DOT',
            'Рекламационны_продукт Индекс',
            'Рекламационны_продукт CAI',
            'Рекламационны_продукт Комментарий',

            'Заявка на вывоз шины Номер',
            'Заявка на вывоз шины Дата создания',
            'Заявка на вывоз шины Перевозчик',
            'Заявка на вывоз шины Стоимость доставки',

            'Акт замены товара Номер',
            'Акт замены товара Дата создания заявки',
            'Акт замены товара Дата отгрузки шины',
            'Акт замены товара Дата получения акта замены товара',

            'Акт возврата шины Номер',
            'Акт возврата шины Дата создания акта',
            'Акт возврата шины Дата отправки акта',
            'Акт возврата шины Дата отгрузки по акту',
            'Акт возврата шины Дата возварта шины',
        ];
    }
}