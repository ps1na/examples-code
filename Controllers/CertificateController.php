<?php

namespace App\Controller\Admin;


use App\Dto\CertificateBrainFilterDto;
use App\Dto\CertificateDto;
use App\Dto\CertificateFilterDto;
use App\Entity\Certificate;
use App\Entity\PaginationLinks;
use App\Entity\UploadFile;
use App\Form\CertificateFilterType;
use App\Form\CertificateForm;
use App\Form\Type\CertificateBrainType;
use App\Repository\CertificateRepository;
use App\Service\CertificateService;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Class CertificateController
 * @Route("/admin/certificate", name="admin_certificate_")
 * @package App\Controller\Admin
 */
class CertificateController extends AbstractController
{
    /**
     * @var CertificateService
     */
    private $certificateService;

    /**
     * @var CertificateRepository
     */
    private $certificateRepository;

    /**
     * @var FilesystemInterface
     *
     */
    private $filesystem;

    /**
     * CertificateController constructor.
     * @param CertificateService $certificateService
     * @param CertificateRepository $certificateRepository
     * @param FilesystemInterface $filesystem
     */
    public function __construct(
        CertificateService $certificateService,
        CertificateRepository $certificateRepository,
        FilesystemInterface $filesystem
    )
    {
        $this->certificateService = $certificateService;
        $this->certificateRepository = $certificateRepository;
        $this->filesystem = $filesystem;
    }

    /**
     * @Route("/list/{page}", defaults={"page"=1}, name="index")
     * @param $page
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function indexAction($page, Request $request)
    {
        $certificateBrainFilter = $this->createForm(CertificateBrainType::class, null);
        $certificateBrainFilter->handleRequest($request);
        /**
         * @var $dto CertificateBrainFilterDto
         */
        $dto = $certificateBrainFilter->getData() ?: new CertificateBrainFilterDto();
        if ($certificateBrainFilter->isSubmitted()) {
            return $this->redirectToRoute('admin_certificate_index');
        }
        $certificatePaginationList = $this->certificateRepository->findByBrainFilter($dto, $page);

        return $this->render('all/certificate/index.html.twig', [
            'certificateBrainFilter' => $certificateBrainFilter->createView(),
            'certificatePaginationList' => $certificatePaginationList
        ]);
    }

    /**
     * @Route("/create", name="create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm(CertificateForm::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            /** @var SubmitButton $resetButton */
            $resetButton = $form->get('cancel');
            if ($resetButton && $resetButton->isClicked()) {
                return $this->redirectToRoute('admin_certificate_index');
            }
        }
        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * @var $certificateDto CertificateDto
             */
            $certificateDto = $form->getData();
            try {
                $this->certificateService->create($certificateDto);
                $this->addFlash('success', 'Сертификат успешно создан');
                return $this->redirectToRoute('admin_certificate_index');
            } catch (\Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
            }
        }

        return $this->render('all/certificate/create.html.twig', [
            'form' => $form->createView(),
            'title' => 'Создание сертификата',
            'h1' => 'Создание сертификата',
            'body_class' => 'admin-certificate-create'
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     * @param Certificate $certificate
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \OutOfBoundsException
     */
    public function editAction(Certificate $certificate, Request $request)
    {
        $certificateDto = $this->setEntityCertificateDto($certificate);

        $form = $this->createForm(CertificateForm::class, $certificateDto);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            /** @var SubmitButton $resetButton */
            $resetButton = $form->get('cancel');
            if ($resetButton && $resetButton->isClicked()) {
                return $this->redirectToRoute('admin_certificate_index');
            }
            $certificateDto = $form->getData();
            try {
                $this->certificateService->update($certificate,$certificateDto);
                $this->addFlash('success', 'Сертификат успешно отредактирован');
                return $this->redirectToRoute('admin_certificate_index');
            } catch (\Exception $exception) {
                $this->addFlash('error', $exception->getMessage());
            }
        }

        return $this->render('all/certificate/create.html.twig', [
            'form' => $form->createView(),
            'title' => 'Редактирование сертификата',
            'h1' => 'Редактирование сертификата',
            'body_class' => 'admin-certificate-edit'
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete")
     * @param Certificate $certificate
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Certificate $certificate)
    {
        try {
            $this->certificateService->delete($certificate);
        } catch (\Exception $exception) {
            $this->addFlash('error', $exception->getMessage());
        }

        $this->addFlash('success', 'Сертификат успешно удалён');
        return $this->redirectToRoute('admin_certificate_index');
    }

    /**
     * @param Certificate $certificate
     * @return CertificateDto
     */
    private function setEntityCertificateDto(Certificate $certificate)
    {
        $certificateDto = new CertificateDto();
        $certificateDto->setNumber($certificate->getNumber());
        $certificateDto->setDescription($certificate->getDescription());
        $certificateDto->setCais($certificate->getCais());
        $certificateDto->setDateTo($certificate->getDateTo());
        $certificateDto->setCais($certificate->getCais());
        $files = $certificate->getFiles();

        $certificateDto->setFilesDto($files);
        return $certificateDto;
    }


    /**
     * @Route("/certificates/generate/{id}", name="certificate_generate")
     * @param UploadFile $file
     * @return BinaryFileResponse
     * @throws FileNotFoundException
     */
    public function generateCertificate(UploadFile $file)
    {
        $content = $this->filesystem->read($file->getFile()->getPathname());
        $tempname = tempnam(sys_get_temp_dir(), 'watermarks') . '.'. $file->getFile()->getExtension();
        file_put_contents($tempname, $content);
        $response = new BinaryFileResponse($tempname);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'Сертификат.'.$file->getFile()->getExtension()
        );

        return $response;
    }
}