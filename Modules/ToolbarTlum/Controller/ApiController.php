<?php

namespace App\Modules\ToolbarTlum\Controller;

use App\Common\Controller\AbstractController;
use App\Modules\ToolbarTlum\Model\ToolbarShared;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends AbstractController
{
    /**
     * @var ToolbarShared
     */
    private $sharedToolbar;

    /**
     * {@inheritdoc}
     */
    public function __construct(Application $app)
    {
        parent::__construct($app);

        $this->sharedToolbar = $app['module.toolbartlum.shared'];
    }

    /**
     * Возвращает html для тулбара.
     *
     * @return JsonResponse
     */
    public function htmlAction()
    {
        return new JsonResponse([
            'code' => JsonResponse::HTTP_OK,
            'html' => $this->sharedToolbar->render(),
        ]);
    }
}
