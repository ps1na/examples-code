<?php

namespace App\Modules\ToolbarTlum\Controller;

use App\Common\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class StaticController extends AbstractController
{
    /**
     * Send generated js script code.
     *
     * @return Response
     */
    public function scriptAction()
    {
        $content = $this->twig->render('@ToolbarTlum/script.js.twig');

        return new Response($content, Response::HTTP_OK, [
            'Content-Type' => 'application/javascript',
        ]);
    }

    /**
     * Send css style.
     *
     * @return Response
     */
    public function styleAction()
    {
        $filepath = __DIR__.'/../Resources/assets/style.css';
        $content = file_get_contents($filepath);

        return new Response($content, Response::HTTP_OK, [
            'Content-Type' => 'text/css',
        ]);
    }
}
