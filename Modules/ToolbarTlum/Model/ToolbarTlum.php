<?php

namespace App\Modules\ToolbarTlum\Model;

class ToolbarTlum extends AbstractToolbar
{
    /**
     * {@inheritdoc}
     */
    public function getContainer()
    {
        $container = (new ToolbarTlumContainer())
            ->addGroup(
                (new ToolbarTlumGroup('Тв', '82px', '100px', '5px', '#'))
                    ->addItem(new ToolbarTlumItem('Мульт', 'http://mult.tlum.ru/?utm_source=bar&utm_medium=tlum&utm_campaign=toolbar'))
                    ->addItem(new ToolbarTlumItem('Мама', 'http://mama.tlum.ru/?utm_source=bar&utm_medium=tlum&utm_campaign=toolbar'))
                    ->addItem(new ToolbarTlumItem('Ani', 'http://ani.tlum.ru/?utm_source=bar&utm_medium=tlum&utm_campaign=toolbar'))
                    ->addItem(new ToolbarTlumItem('Тлум HD', 'https://tlum.ru/club-event/uze-skoro-detskij-kanal-tlum-hd/?utm_source=bar&utm_medium=tlum&utm_campaign=toolbar'))
            )
            ->addGroup(
                (new ToolbarTlumGroup('Кино', '115px', '80px', '5px', '#'))
                    ->addItem(new ToolbarTlumItem('Мульт в кино', 'https://multvkino.tlum.ru/?utm_source=bar&utm_medium=tlum&utm_campaign=2016'))
                    ->addItem(new ToolbarTlumItem('Тлум афиша', 'http://tlum.ru/club-events/section/watch/?utm_source=bar&utm_medium=tlum&utm_campaign=toolbar'))
                    ->addItem(new ToolbarTlumItem('Обсудить', 'https://www.u-mama.ru/forum/hobby/cinema/?utm_source=tlum.ru&utm_medium=menu&utm_campaign=12_2016'))
            )
            ->addGroup(
                (new ToolbarTlumGroup('Смотреть', '170px', '60px', '5px', '#'))
                    ->addItem(new ToolbarTlumItem('Мультфильмы онлайн', 'https://tlum.ru/watch/playlists/?utm_source=bar&utm_medium=tlum&utm_campaign=toolbar'))
                    ->addItem(new ToolbarTlumItem('Приложение Мульт', 'http://i-mult.ru/games/mult/?utm_source=tlumbar&utm_campaign=mult'))
            )
            ->addGroup(
                (new ToolbarTlumGroup('Праздники', '142px', '125px', '5px', '#'))
                    ->addItem(new ToolbarTlumItem('Мультимир', 'http://multimir.tv/?utm_source=bar&utm_medium=tlum&utm_campaign=toolbar'))
                    ->addItem(new ToolbarTlumItem('Парад колясок', 'http://mama.tlum.ru/parad/?utm_source=bar&utm_medium=tlum&utm_campaign=toolbar'))
                    ->addItem(new ToolbarTlumItem('Яркое лето', 'http://mult.tlum.ru/summer/?utm_source=bar&utm_medium=tlum&utm_campaign=toolbar'))
                    ->addItem(new ToolbarTlumItem('Тлум афиша', 'http://tlum.ru/club-events/section/go/?utm_source=bar&utm_medium=tlum&utm_campaign=toolbar'))
                    ->addItem(new ToolbarTlumItem('Новогодние елки', 'https://tlum.ru/news/lucsie-novogodnie-elki-moskvy/?utm_source=bar&utm_campaign=toolbar&utm_medium=tlum&utm_content=ny17tlum'))
            )
            ->addGroup(
                (new ToolbarTlumGroup('Игры', '156px', '147px', '5px', '#'))
                    ->addItem(new ToolbarTlumItem('Ми-ми-мишки', ' http://i-mult.ru/games/mimimishki/?utm_source=tlumbar&utm_campaign=bebebears'))
                    ->addItem(new ToolbarTlumItem('Бумажки', ' http://i-mult.ru/games/bumazhki/?utm_source=tlumbar&utm_campaign=papermates'))
                    ->addItem(new ToolbarTlumItem('Сказочный патруль', ' http://i-mult.ru/games/patrol/?utm_source=tlumbar&utm_campaign=patrol'))
                    ->addItem(new ToolbarTlumItem('Слова', 'http://i-mult.ru/games/words/?utm_source=tlumbar&utm_campaign=words'))
                    ->addItem(new ToolbarTlumItem('Аркадий паровозов', 'http://i-mult.ru/games/parovozov/?utm_source=tlumbar&utm_campaign=parovozov'))
                    ->addItem(new ToolbarTlumItem('Другие игры', 'http://tlum.ru/club-events/section/play/?utm_source=bar&utm_medium=tlum&utm_campaign=toolbar'))
            )
            ->addGroup(
                new ToolbarTlumGroup('Мультимир', '0px', '0px', '0px', 'http://multimir.tv/?utm_source=bar&utm_campaign=multimir-2017&utm_medium=tlum&utm_content=toolbar-redbtn', '#ef0040')
            )
        ;

        return $container;
    }
}
