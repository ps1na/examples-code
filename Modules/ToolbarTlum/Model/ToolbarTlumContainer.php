<?php

namespace App\Modules\ToolbarTlum\Model;

class ToolbarTlumContainer
{
    /**
     * @var ToolbarTlumGroup[]
     */
    private $groups = [];
    /**
     * @var string
     */
    private $logo;

    public function getLogo()
    {
        return $this->logo;
    }

    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }
    /**
     * @param ToolbarTlumGroup $group
     *
     * @return $this
     */
    public function addGroup(ToolbarTlumGroup $group)
    {
        $this->groups[] = $group;

        return $this;
    }

    /**
     * @return ToolbarTlumGroup[]
     */
    public function getGroups()
    {
        return $this->groups;
    }
}
