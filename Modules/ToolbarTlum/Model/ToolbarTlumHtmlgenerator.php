<?php

namespace App\Modules\ToolbarTlum\Model;

class ToolbarTlumHtmlgenerator
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * Constructor.
     *
     * @param \Twig_Environment $twig
     */
    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @param ToolbarTlumContainer $container
     *
     * @return string
     */
    public function generate(ToolbarTlumContainer $container)
    {
        return $this->twig->render('@ToolbarTlum/toolbar.html.twig', [
            'container' => $container,
        ]);
    }
}
