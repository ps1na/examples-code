<?php

namespace App\Modules\ToolbarTlum\Model;

class ToolbarTlumCodeGenerator
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * Constructor.
     *
     * @param \Twig_Environment $twig
     */
    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * Generate script html code.
     *
     * @return string
     */
    public function generate()
    {
        return $this->twig->render('@ToolbarTlum/code.html.twig');
    }
}
