<?php

namespace App\Modules\ToolbarTlum\Model;

class ToolbarShared extends AbstractToolbar
{
    /**
     * {@inheritdoc}
     */
    public function getContainer()
    {
        $container = (new ToolbarTlumContainer())
            ->addGroup(
                (new ToolbarTlumGroup('Смотреть', '180px', '186px', '5px', '#'))
                    ->addItem(new ToolbarTlumItem('Мульт', 'http://mult.tlum.ru/?utm_source=bar&utm_medium=others&utm_campaign=toolbar'))
                    ->addItem(new ToolbarTlumItem('Приложение Мульт', 'http://i-mult.ru/games/mult/?utm_source=tlumbar&utm_campaign=mult'))
                    ->addItem(new ToolbarTlumItem('Мульт в кино', 'https://multvkino.tlum.ru/?utm_source=bar&utm_medium=tlum&utm_campaign=2016'))
                    ->addItem(new ToolbarTlumItem('Мультимузыка', 'http://mult-music.tlum.ru/?utm_source=bar&utm_medium=others&utm_campaign=toolbar'))
                    ->addItem(new ToolbarTlumItem('Тлум HD', 'http://tlum.ru/club-event/uze-skoro-detskij-kanal-tlum-hd/?utm_source=bar&utm_medium=others&utm_campaign=toolbar'))
                    ->addItem(new ToolbarTlumItem('Ani', 'http://ani.tlum.ru/?utm_source=bar&utm_medium=others&utm_campaign=toolbar'))
                    ->addItem(new ToolbarTlumItem('Мама', 'http://mama.tlum.ru/?utm_source=bar&utm_medium=others&utm_campaign=toolbar'))
                    ->addItem(new ToolbarTlumItem('Мультфильмы онлайн', 'https://tlum.ru/watch/playlists/?utm_source=bar&utm_medium=others&utm_campaign=toolbar'))
            )
            ->addGroup(
                (new ToolbarTlumGroup('Сходить', '150px', '117px', '5px', '#'))
                    ->addItem(new ToolbarTlumItem('Мультимир', 'http://multimir.tv/?utm_source=bar&utm_medium=others&utm_campaign=toolbar'))
                    ->addItem(new ToolbarTlumItem('Парад колясок', 'http://mama.tlum.ru/parad/?utm_source=bar&utm_medium=others&utm_campaign=toolbar'))
                    ->addItem(new ToolbarTlumItem('Яркое лето', 'http://mult.tlum.ru/summer/?utm_source=bar&utm_medium=others&utm_campaign=toolbar'))
                    ->addItem(new ToolbarTlumItem('Мульт в кино', 'https://multvkino.tlum.ru/?utm_source=bar&utm_medium=tlum&utm_campaign=2016'))
                    ->addItem(new ToolbarTlumItem('Новогодние елки', 'https://tlum.ru/news/lucsie-novogodnie-elki-moskvy/?utm_source=bar&utm_campaign=toolbar&utm_medium=others&utm_content=ny17tlum'))
            )
            ->addGroup(
                (new ToolbarTlumGroup('Играть', '156px', '147px', '5px', '#'))
                    ->addItem(new ToolbarTlumItem('Ми-ми-мишки', ' http://i-mult.ru/games/mimimishki/?utm_source=tlumbar&utm_campaign=bebebears'))
                    ->addItem(new ToolbarTlumItem('Бумажки', ' http://i-mult.ru/games/bumazhki/?utm_source=tlumbar&utm_campaign=papermates'))
                    ->addItem(new ToolbarTlumItem('Сказочный патруль', ' http://i-mult.ru/games/patrol/?utm_source=tlumbar&utm_campaign=patrol'))
                    ->addItem(new ToolbarTlumItem('Слова', 'http://i-mult.ru/games/words/?utm_source=tlumbar&utm_campaign=words'))
                    ->addItem(new ToolbarTlumItem('Аркадий паровозов', 'http://i-mult.ru/games/parovozov/?utm_source=tlumbar&utm_campaign=parovozov'))
                    ->addItem(new ToolbarTlumItem('Другие игры', 'http://tlum.ru/club-events/section/play/?utm_source=bar&utm_medium=tlum&utm_campaign=toolbar'))
            )
            ->addGroup(
                new ToolbarTlumGroup('Читать', '0px', '0px', '0px', 'http://tlum.ru/club-events/section/read/?utm_source=bar&utm_medium=tlum&utm_campaign=toolbar')
            )
            ->addGroup(
                new ToolbarTlumGroup('Узнать', '0px', '0px', '0px', 'http://tlum.ru/news/?utm_source=bar&utm_medium=tlum&utm_campaign=toolbar')
            )
            ->addGroup(
                new ToolbarTlumGroup('Мультимир', '0px', '0px', '0px', 'http://multimir.tv/?utm_source=bar&utm_campaign=multimir-2017&utm_medium=others&utm_content=toolbar-redbtn', '#ef0040')
            )
            ->setLogo('1')
        ;

        return $container;
    }
}
