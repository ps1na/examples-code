<?php

namespace App\Modules\ToolbarTlum\Model;

class ToolbarTlumGroup
{
    /**
     * @var string
     */
    private $title = '';

    /**
     * @var string
     */
    private $width = '';

    /**
     * @var string
     */
    private $height = '';

    /**
     * @var string
     */
    private $border = '';

    /**
     * @var ToolbarTlumItem[]
     */
    private $items = [];

    /**
     * @var string
     */
    private $url = '';

    /**
     * @var string
     */
    private $color;

    /**
     * Constructor.
     *
     * @param $title
     * @param $width
     * @param $height
     * @param $border
     * @param $url
     */
    public function __construct($title, $width, $height, $border, $url, $color = '#453f4d')
    {
        $this->title = trim((string)$title);
        $this->width = trim((string)$width);
        $this->height = trim((string)$height);
        $this->border = trim((string)$border);
        $this->url = trim((string)$url);
        $this->color = trim((string)$color);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return (string)$this->title;
    }

    /**
     * @param ToolbarTlumItem $item
     *
     * @return $this
     */
    public function addItem(ToolbarTlumItem $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * @return ToolbarTlumItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return string
     */
    public function getWidth()
    {
        return (string)$this->width;
    }

    /**
     * @return string
     */
    public function getHeight()
    {
        return (string)$this->height;
    }

    /**
     * @return string
     */
    public function getBorder()
    {
        return (string)$this->border;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return (string)$this->url;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }
}
