<?php

namespace App\Modules\ToolbarTlum\Model;

abstract class AbstractToolbar
{
    /**
     * @var ToolbarTlumHtmlgenerator
     */
    private $htmlgenerator;

    /**
     * Constructor.
     *
     * @param ToolbarTlumHtmlgenerator $htmlgenerator
     */
    public function __construct(ToolbarTlumHtmlgenerator $htmlgenerator)
    {
        $this->htmlgenerator = $htmlgenerator;
    }

    /**
     * @return ToolbarTlumContainer
     */
    abstract public function getContainer();

    /**
     * @return string
     */
    public function render()
    {
        return $this->htmlgenerator->generate($this->getContainer());
    }
}
