<?php

namespace App\Modules\ToolbarTlum\Model;

class ToolbarTlumItem
{
    /**
     * @var string
     */
    private $title = '';

    /**
     * @var string
     */
    private $url = '';

    /**
     * Constructor.
     *
     * @param string $title
     * @param string $url
     */
    public function __construct($title, $url)
    {
        $this->title = trim((string)$title);
        $this->url = trim((string)$url);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return (string)$this->title;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return (string)$this->url;
    }
}
