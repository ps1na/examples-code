<?php

namespace App\Modules\ToolbarTlum;

use App\Common\Module\Module;
use App\Modules\ToolbarTlum\Controller\ApiController;
use App\Modules\ToolbarTlum\Controller\StaticController;
use App\Modules\ToolbarTlum\Model\ToolbarShared;
use App\Modules\ToolbarTlum\Model\ToolbarTlum;
use App\Modules\ToolbarTlum\Model\ToolbarTlumCodeGenerator;
use App\Modules\ToolbarTlum\Model\ToolbarTlumHtmlgenerator;
use Silex\Application;

/**
 * Provides toolbar tlum service.
 */
class ToolbarTlumModule extends Module
{
    /**
     * {@inheritdoc}
     */
    public function onRegister()
    {
        $app = $this->app;
        $this->addTwigNamespace(__DIR__.'/Resources/views', 'ToolbarTlum');
        $apiCollection = $this->getApiControllers();
        $webCollection = $this->getWebControllers();

        $app['module.toolbartlum.api_controller'] = $app::share(function (Application $app) {
            return new ApiController($app);
        });

        $app['module.toolbartlum.static_controller'] = $app::share(function (Application $app) {
            return new StaticController($app);
        });

        $app['module.toolbartlum.code_generate'] = $app::share(function (Application $app) {
            return new ToolbarTlumCodeGenerator($app['twig']);
        });

        $app['module.toolbartlum.html_generator'] = $app::share(function (Application $app) {
            return new ToolbarTlumHtmlgenerator($app['twig']);
        });

        $app['module.toolbartlum.tlum'] = $app::share(function (Application $app) {
            return new ToolbarTlum($app['module.toolbartlum.html_generator']);
        });

        $app['module.toolbartlum.shared'] = $app::share(function (Application $app) {
            return new ToolbarShared($app['module.toolbartlum.html_generator']);
        });

        /** @noinspection PhpUndefinedMethodInspection */
        $apiCollection->match(
            '/v1/toolbar/',
            'module.toolbartlum.api_controller:htmlAction'
        )
            ->method('OPTIONS|GET')
            ->bind('api.toolbartlum.html')
            ->cache(['expires' => new \DateTime('+2 week'), 'public' => true])
            ->cors()
        ;

        /** @noinspection PhpUndefinedMethodInspection */
        $webCollection->match(
            '/toolbar/script.js',
            'module.toolbartlum.static_controller:scriptAction'
        )
            ->method('OPTIONS|GET')
            ->bind('web.toolbartlum.script')
            ->cache(['expires' => new \DateTime('+2 week'), 'public' => true])
            ->cors()
        ;

        /** @noinspection PhpUndefinedMethodInspection */
        $webCollection->match(
            '/toolbar/style.css',
            'module.toolbartlum.static_controller:styleAction'
        )
            ->method('OPTIONS|GET')
            ->bind('web.toolbartlum.style')
            ->cache(['expires' => new \DateTime('+2 week'), 'public' => true])
            ->cors()
        ;
    }
}
