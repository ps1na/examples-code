<?php

namespace App\Modules\Homepage_v3\Exceptions;

use InvalidArgumentException;

class EntityNotFoundException extends InvalidArgumentException
{
    /**
     * EntityNotFoundException constructor.
     *
     * @param string          $message
     * @param int             $code
     * @param \Exception|null $previous
     */
    public function __construct(
        $message = 'Entity was not found.',
        $code = 404,
        \Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
