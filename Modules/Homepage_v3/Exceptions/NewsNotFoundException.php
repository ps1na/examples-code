<?php
/**
 * Created by PhpStorm.
 * User: Marakyschev
 * Date: 15.06.17
 * Time: 13:27.
 */

namespace App\Modules\Homepage_v3\Exceptions;

use InvalidArgumentException;

class NewsNotFoundException extends InvalidArgumentException
{
    /**
     * ProjectNotFoundException constructor.
     *
     * @param string          $message
     * @param int             $code
     * @param \Exception|null $previous
     */
    public function __construct(
        $message = 'News was not found.',
        $code = 404,
        \Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
