<?php

namespace App\Modules\Homepage_v3\Controller;

use App\Common\Controller\AbstractController;
use App\Common\Entity\PaginationOptions;
use App\Modules\Animation\Model\ListFilter;
use App\Modules\Animation\Model\ListOrder;
use App\Modules\Animation\Repository\AnimationRepository;
use App\Modules\Character\Repository\CharacterWithExistingProductsRepository;
use App\Modules\ClubEvent\Model\ListParams;
use App\Modules\ClubEvent\Model\ListParamsExtractor;
use App\Modules\ClubTest\Repository\ClubTestRepository;
use App\Modules\Homepage_v3\Model\CalendarHomepage;
use App\Modules\Homepage_v3\Model\Idea;
use App\Modules\Homepage_v3\Model\IdeaType;
use App\Modules\Homepage_v3\Model\News;
use App\Modules\Homepage_v3\Model\Video;
use App\Modules\Homepage_v3\Repository\NewsRepository;
use App\Modules\Homepage_v3\Repository\PlaybillRepository;
use App\Modules\Homepage_v3\Repository\ProjectRepository;
use App\Modules\Homepage_v3\Repository\PromoblockItemRepository;
use App\Modules\Homepage_v3\Repository\VideoParentRepository;
use App\Modules\Homepage_v3\Repository\VideoRepository;
use App\Modules\IdiberiProduct\Controller\LatestProductsWidgetController;
use App\Modules\News\Model\NewsListFilter;
use App\Modules\ProductStoreOffers\Models\ServiceGetStoreProduct;
use App\Modules\Tag\Model\TagTypeEntity;
use App\Modules\Video\Exceptions\VideoPlaylistNotFoundException;
use App\Modules\Votes\Entity\VoteListFilter;
use App\Modules\Votes\Entity\VoteListOrder;
use App\Modules\Votes\Entity\VoteScriptGenerator;
use App\Modules\Votes\Repository\VotesRepository;
use App\TlumApp;
use phpDocumentor\Reflection\TypeResolver;
use Repositories\ClubEventRepository;
use Repositories\ClubEventTypeRepositoryInterface;
use Repositories\IdiberiImageRepository;
use Repositories\IdiberiProductBrandRepository;
use Repositories\IdiberiProductCategoryRepository;
use Repositories\IdiberiProductRepository;
use Repositories\TagGroupRepository;
use Repositories\TagRepository;
use Service\UserLocationDetector\UserLocationDetector;
use Service\WebClubEventTypeResolver\WebClubEventTypeResolver;
use Silex\Application;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class WebHomepageController extends AbstractController
{
    const TAG_GENDER_BOY = 17;
    const TAG_GENDER_GIRL = 18;
    const TAG_AGE_GROUP = 5;
    const TAG_INTEREST_GROUP = 6;
    /**
     * @var PromoblockItemRepository
     */
    private $promoblockItemRepository;

    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * @var NewsRepository
     */
    private $homepageNewsRepository;

    /**
     * @var AnimationRepository
     */
    private $animationRepository;

    /**
     * @var bool
     */
    private $isVagrant;

    /**
     * @var VideoRepository
     */
    private $homepageVideoRepository;

    /**
     * @var TagGroupRepository
     */
    private $tagGroupRepository;

    /**
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * @var \Repositories\NewsRepository
     */
    private $newsRepository;

    /**
     * @var VideoParentRepository
     */
    private $homepageVideoParentRepository;

    /**
     * @var ClubEventRepository
     */
    private $clubEventRepository;

    /**
     * @var PlaybillRepository
     */
    private $playbillRepository;

    /**
     * @var WebClubEventTypeResolver
     */
    private $typeResolver;

    /**
     * @var LatestProductsWidgetController
     */
    private $latestProductsWidgetController;

    /**
     * @var CharacterWithExistingProductsRepository;
     */
    private $characterWithExistingsProductsRepository;

    /**
     * @var IdiberiProductBrandRepository
     */
    private $idiberiProductBrandRepository;

    /**
     * @var IdiberiProductRepository
     */
    private $idiberiProductRepository;

    /**
     * @var VotesRepository
     */
    private $votesRepository;

    /**
     * @var ClubTestRepository
     */
    private $clubTestsRepository;

    /**
     * @var UserLocationDetector
     */
    private $userLocationDetector;

    /**
     * @var IdiberiImageRepository
     */
    private $idiberiImageRepository;

    /**
     * @var ListParamsExtractor
     */
    private $listParamExtractor;

    /**
     * @var ClubEventTypeRepositoryInterface
     */
    private $clubEventTypeRepository;

    /**
     * @var VoteScriptGenerator
     */
    private $voteScriptGenerator;

    /**
     * @var IdiberiProductCategoryRepository
     */
    private $idiberiProductCategoryRepository;

    /**
     * @var ServiceGetStoreProduct
     */
    private $serviceGetStoreProduct;

    /**
     * @var \MongoDB
     */
    private $mongoDb;

    /**
     * WebHomepageController constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->promoblockItemRepository = $app['module.homepage.promoblock.repository'];
        $this->projectRepository = $app['module.homepage.project.repository'];
        $this->homepageNewsRepository = $app['module.homepage.news.repository'];
        $this->animationRepository = $app['module.animation.repository'];
        $this->homepageVideoRepository = $app['module.homepage.video.repository'];
        $this->tagGroupRepository = $app['tag_group_repository'];
        $this->tagRepository = $app['tag_repository'];
        $this->newsRepository = $app['news_repository'];
        $this->homepageVideoParentRepository = $app['module.homepage.video_parent.repository'];
        $this->clubEventRepository = $app['module.club_event.repository'];
        $this->playbillRepository = $app['module.homepage.playbill.repository'];
        $this->typeResolver = $app['club_event_type.resolver'];
        $this->latestProductsWidgetController = $app['module.idiberi_product.latest_products_widget_controller'];
        $this->isVagrant = $app['_env'] === TlumApp::ENV_VAGRANT;
        $this->characterWithExistingsProductsRepository = $app['module.character.with_existings_products_repository'];
        $this->idiberiProductBrandRepository = $app['idiberi_product_brand_repository'];
        $this->idiberiProductRepository = $app['idiberi_product_repository'];
        $this->votesRepository = $app['votes.repository'];
        $this->clubTestsRepository = $app['module.club_test.repository'];
        $this->userLocationDetector = $app['user_location.detector'];
        $this->idiberiImageRepository = $app['idiberi_image_repository'];
        $this->listParamExtractor = $app['module.club_event.list_params_extractor'];
        $this->clubEventTypeRepository = $app['club_event_type_repository'];
        $this->voteScriptGenerator = $app['votes.script_generator'];
        $this->idiberiProductCategoryRepository = $app['idiberi_product_category_repository'];
        $this->serviceGetStoreProduct = $app['service.get.store.product'];
        $this->mongoDb = $app['mongodb.idiberi_mongo'];
    }

    /**
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('@Homepage_v3/web/index.html.twig');
    }

    /**
     * @return Response
     */
    public function renderModulePromoblock()
    {
        $promoblockItems1 = $this->promoblockItemRepository->findAll();

        $promoblockItems2 = $promoblockItems1;
        $lastEl = array_shift($promoblockItems2);
        $promoblockItems2[] = $lastEl;

        $promoblockItems3 = $promoblockItems2;
        $lastEl = array_shift($promoblockItems3);
        $promoblockItems3[] = $lastEl;

        $promoblockItems4 = $promoblockItems3;
        $lastEl = array_shift($promoblockItems4);
        $promoblockItems4[] = $lastEl;

        return $this->render('@Homepage_v3/web/promoblock/promoblock_index.html.twig', [
            'promoblockItems1' => $promoblockItems1,
            'promoblockItems2' => $promoblockItems2,
            'promoblockItems3' => $promoblockItems3,
            'promoblockItems4' => $promoblockItems4,
        ]);
    }

    /**
     * @param Request     $request
     * @param Application $app
     *
     * @return Response
     */
    public function renderModuleProduct(Request $request, Application $app)
    {
        $new_products = [];
        $form = [
            'tag_gender' => (int)$request->get('tag_gender'),
            'tags' => (array)$request->get('tags'),
        ];

        $limit = 7;
        if ($this->isVagrant) {
            $products = $this->latestProductsWidgetController->getTestProductsList($limit)->getItems();
            foreach ($products as $product) {
                $product->setStoreOfferLink('#123');
                $product->setAvailable(true);
                $product->setPrice(mt_rand(0, 2000));
                $product->setStoreName('ozon.ru');
                $new_products[] = $product;
            }
            $products = $new_products;
        } else {
            $products = $this->getDataFromIdiberiForProducts($app);
            $products = $this->serviceGetStoreProduct->convert($products);
        }

        return $this->render('@Homepage_v3/web/product/product_index.html.twig', [
            'products' => $products,
            'form' => $form,
            'tags_gender_boy' => self::TAG_GENDER_BOY,
            'tags_gender_girl' => self::TAG_GENDER_GIRL,
            'ages' => $this->tagRepository->findByGroupId(self::TAG_AGE_GROUP, ['id', 'asc'])['items'],
            'brands' => $this->idiberiProductBrandRepository->find()['items'],
            'characters' => $this->characterWithExistingsProductsRepository->findCharactersWithExistingProducts(),
        ]);
    }

    /**
     * @return Response
     */
    public function renderModuleProject()
    {
        $projects = $this->projectRepository->findAll();

        return $this->render('@Homepage_v3/web/project/project_index.html.twig', [
            'projects' => $projects,
        ]);
    }

    /**
     * @return Response
     */
    public function renderModuleGiftIdeas()
    {
        return $this->render('@Homepage_v3/web/giftideas/giftideas_index.html.twig', [
           'ideas' => $this->isVagrant ? [] : $this->getFirstIdeas(),
        ]);
    }

    /**
     * @return Response
     */
    public function renderModuleNews()
    {
        $slug = 'novosti';
        $limit = 9;
        $homepageNewsAll = $this->homepageNewsRepository->findAll();
        $homepageNews = $this->isPublishedEntity($homepageNewsAll);
        $currentCount = count($homepageNews);
        $news = $this->getNews($slug, $limit);
        $i = 0;
        while ($currentCount < 9) {
            $oneNews = $this->homepageNewsRepository->getBlank();
            $oneNews->setNews($news[$i]);
            $homepageNews[] = $oneNews;
            ++$currentCount;
            ++$i;
        };

        return $this->render('@Homepage_v3/web/news/news_index.html.twig', [
            'news' => $homepageNews,
        ]);
    }

    /**
     * @return Response
     */
    public function renderModuleAnimation()
    {
        $animationFilter = new ListFilter();
        $animationFilter->setPublished(true);
        $animationFilter->setTypeId(TagTypeEntity::ANIMATION);
        $listOrder = (new ListOrder())->setFavorites();
        $animations = $this->animationRepository->findAll($animationFilter, $listOrder);
        $i = 0;
        // для сохранения целостности верстки главной страницы
        if ($this->isVagrant) {
            $animation = reset($animations);
            do {
                $animations[] = $animation;
                ++$i;
            } while ($i < 17);
        }

        return $this->render('@Homepage_v3/web/animation/animation_index.html.twig', [
            'animations' => $animations,
        ]);
    }

    /**
     * @return Response
     */
    public function renderModuleVideo()
    {
        $slug = 'smotret';
        $videosAll = $this->homepageVideoRepository->findAll();
        $videosPublished = $this->isPublishedEntity($videosAll);
        $videos = $this->isMainVideo($videosPublished);
        $news = $this->getNews($slug);

        return $this->render('@Homepage_v3/web/video/video_index.html.twig', [
            'videos' => $videos,
            'news' => $news,
        ]);
    }

    /**
     * @return Response
     */
    public function renderModuleVideoParent()
    {
        $slug = 'uznat';
        $videosAll = $this->homepageVideoParentRepository->findAll();
        $videosPublished = $this->isPublishedEntity($videosAll);
        $videos = $this->isMainVideo($videosPublished);
        $news = $this->getNews($slug);

        return $this->render('@Homepage_v3/web/videoparent/video_index.html.twig', [
            'videos' => $videos,
            'news' => $news,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function renderModulePlaybill(Request $request)
    {
        $playbillsAll = $this->playbillRepository->findAll();
        $playbills = $this->isPublishedEntity($playbillsAll);
        $calendarHomepage = (new CalendarHomepage())->getCalendar();
        $currentCount = count($playbills);
        $listParams = $this->listParamExtractor->extract($request);
        $listParams->getPaginationOptions()->setLimit(3);
        $offset = $listParams->getPaginationOptions()->getOffset();
        $limit = $listParams->getPaginationOptions()->getLimit();
        $clubEvents = $this->clubEventRepository->findForWeb(
            $offset,
            $limit + 1,
            $this->getSearchParams($listParams)
        );
        foreach (array_slice($clubEvents, 0, 3) as $clubEvent) {
            $playbill = $this->playbillRepository->getBlank();
            $playbill->setClubEvent($clubEvent);
            $playbills[] = $playbill;
        }

        return $this->render('@Homepage_v3/web/playbill/playbill_index.html.twig', [
            'playbills' => $playbills,
            'dateWeek' => $calendarHomepage,
            'types' => $this->clubEventTypeRepository->findAll(),
        ]);
    }

    /**
     * @return Response
     */
    public function renderModuleVotesAndTests()
    {
        $paginationOptions = new PaginationOptions();
        $paginationOptions->setLimit(3);
        $votesListFilter = new VoteListFilter();
        $votesListFilter->setPublished(true);
        $listOrder = new VoteListOrder();
        $listOrder->setByPublishedAtDesc();
        $vote = $this->votesRepository->find($paginationOptions, $votesListFilter, $listOrder)->getItems()[0];

        $paginationOptions->setLimit(6);
        $clubTestListFilter = new \App\Modules\ClubTest\Model\ListFilter();
        $clubTestListFilter->setPublished(true);
        $clubTests = $this->clubTestsRepository->find($paginationOptions, $clubTestListFilter, $listOrder)->getItems();
        $clubTestMain = array_shift($clubTests);

        $scriptVote = $this->voteScriptGenerator->generateScript($vote->getId());

        return $this->render('@Homepage_v3/web/votes_and_tests/main_index.html.twig', [
            'mainClubTest' => $clubTestMain,
            'clubTests' => $clubTests,
            'vote' => $scriptVote,
        ]);
    }

    /**
     * @return Response
     */
    public function renderModuleOnline()
    {
        $limit = 8;
        if ($this->isVagrant) {
            $products = $this->latestProductsWidgetController->getTestProductsList($limit)->getItems();
        } else {
            $idCategory = '54edbc6e761b536d0a0041b8';
            $lastId = null;
            $skip = 0;
            $categories = $this->idiberiProductCategoryRepository->getChildCategory($idCategory);
            $categories[] = $idCategory;
            $params['categories'] = $categories;
            $params['sort'] = 'available';
            $params['sort_order'] = -1;
            $params['sort2'] = true;
            $products = $this->idiberiProductRepository->find($lastId, $limit, $params, $skip)['items'];
        }

        return $this->render('@Homepage_v3/web/product/online_index.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function indexActionIdeasProducts(Application $app, Request $req, $id)
    {
        $idea = $this->getDataFromIdiberiForIdieasId($app, $req, $id);
        if (count($idea->getProducts()) === 0) {
            return new RedirectResponse($this->urlGenerator->generate('ideas.web.products', [
                'id' => $idea->getId(),
            ]));
        }

        $prevPage = false;
        $nextPage = false;
        $offset = 0;
        if ($req->query->get('page') > 1) {
            $prevPage = $req->query->get('page') - 1;
        }

        if ($idea->isHasMore()) {
            $nextPage = $req->query->get('page') + 1;
        }

        if (!$req->query->get('page')) {
            $nextPage = $req->query->get('page') + 2;
        }

        $products = $app['service.get.store.product']->convert($idea->getProducts());
        $idea->setProducts($products);

        return $this->render('@Homepage_v3/web/giftideas/giftideas_products_index.html.twig', [
            'idea' => $idea,
            'prevPage' => $prevPage,
            'nextPage' => $nextPage,
            'offset' => $offset,
        ]);
    }

    /**
     * @param     $slug
     * @param int $limit
     *
     * @return \Entities\NewsEntity[]
     */
    private function getNews($slug, $limit = 6)
    {
        $tagGroup = $this->tagGroupRepository->findBySlug((string)$slug);
        $paginationOption = new PaginationOptions();
        $paginationOption->setLimit($limit);
        $newsListFilter = new NewsListFilter();
        // Для вагрната , заполняем "пустышками"
        if (!$this->isVagrant) {
            $newsListFilter->setWithTagGroups([$tagGroup]);
        }
        $news = $this->newsRepository->findForWebPaginatedList($paginationOption, $newsListFilter)->getItems();

        return $news;
    }

    /**
     * @return array
     */
    private function getDataFromIdiberiForProducts(Application $app)
    {
        $limit = 7;
        $servName = '';
        if ($app['_env'] === TlumApp::ENV_STAGING) {
            $servName = 'dev.';
        }
        if ($app['_env'] === TlumApp::ENV_PROD) {
            $servName = '';
        }
        if ($app['_env'] === TlumApp::ENV_STAGING_2) {
            $servName = 'stage2.';
        }
        // USE LOCAL REPOSITORY BECAUSE THIS IS FAST
        $url = 'https://'.$servName.'api.idiberi.ru/giftsSearch/list';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
        $response = curl_exec($ch);
        curl_close($ch);
        $resultItems = json_decode($response, true);
        $produts = [];
        foreach ($resultItems as $key => $item) {
            if ($key < $limit) {
                $produts[] = $this->idiberiProductRepository->findById($item['product']['id']);
            }
        }

        return $produts;
    }

    /**
     * @return Idea[]
     */
    private function getFirstIdeas()
    {
        $collection = $this->mongoDb->selectCollection('idiberi_gift');
        $cursor = $collection->find(['isDeleted' => false])->sort(['position' => 1])->limit(6);

        $result = [];
        foreach ($cursor as $doc) {
            $idea = new Idea($this->urlGenerator);
            $idea->setId((string)$doc['_id']);
            $idea->setTitle($doc['title']);
            if (isset($doc['url'])) {
                $idea->setUrl($doc['url']);
            }
            $idea->setImageDestop($this->idiberiImageRepository->findById((string)$doc['picture']['$id']));
            $idea->setImageAdaptive($this->idiberiImageRepository->findById((string)$doc['adaptivePicture']['$id']));
            $idea->setImageTablet($this->idiberiImageRepository->findById((string)$doc['tabletPicture']['$id']));
            $idea->setPosition($doc['position']);
            $result[] = $idea;
        }

        return $result;
    }

    /**
     * FIXME
     * В репозитроо при findAll сделать where с News/Video/Event isPublished.
     *
     * @param $item
     *
     * @return array
     */
    private function isPublishedEntity($items)
    {
        $newItems = [];
        foreach ($items as $key => $item) {
            if ($item->isPublished()) {
                $newItems[] = $item;
            }
        }

        return $newItems;
    }

    /**
     * FIXME.
     *
     * @param $videos
     *
     * @return array
     */
    private function isMainVideo($videos)
    {
        $newVideos = [];
        foreach ($videos as $key => $video) {
            try {
                if ($video->getVideo()->getMainPlaylist()) {
                    $newVideos[] = $video;
                }
            } catch (VideoPlaylistNotFoundException $e) {
            }
        }

        return $newVideos;
    }

    /**
     * @return Idea|RedirectResponse
     */
    public function getDataFromIdiberiForIdieasId(Application $app, Request $request, $id)
    {
        $servName = 'stage2.';
        if ($app['_env'] === TlumApp::ENV_STAGING) {
            $servName = 'dev.';
        }
        if ($app['_env'] === TlumApp::ENV_PROD) {
            $servName = '';
        }
        if ($app['_env'] === TlumApp::ENV_STAGING_2) {
            $servName = 'stage2.';
        }
        // USE LOCAL REPOSITORY BECAUSE THIS IS FAST
        $url = 'https://'.$servName.'api.idiberi.ru/gifts/'.$id;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
        $response = curl_exec($ch);
        curl_close($ch);
        $ideaResponse = json_decode($response, true);
        $limit = 16;
        $reqPage = $request->query->get('page');
        if ($reqPage) {
            $skip = ($limit * $request->query->get('page')) - $limit;
        } else {
            $skip = 0;
        }
        $i = 0;
        $ideaType = new IdeaType($ideaResponse);
        $idea = new Idea($this->urlGenerator);
        if (!isset($ideaResponse['id'])) {
            throw new NotFoundHttpException('Идея не найдена');
        }
        $idea->setTitle($ideaResponse['title']);
        $idea->setId($ideaResponse['id']);

        switch ($ideaType->whatTheType()) {
            case 0:
                $lastId = null;

                $params['category'] = $ideaResponse['category']['id'];
                $params['sort'] = 'modifiedAt';
                $params['sort_order'] = -1;
                $params['sort2'] = true;
                $products = $this->idiberiProductRepository->find($lastId, $limit, $params, $skip);
                foreach ($products['items'] as $product) {
                    $idea->addProduct($product);
                }
                break;
            case 1:
                foreach ($ideaResponse['product_ids'] as $k => $productId) {
                    if ($skip <= $k && $i < $limit) {
                        $product = $this->idiberiProductRepository->findById($productId);
                        $idea->addProduct($product);
                        ++$i;
                    }
                }
                $all = $skip + $i;
                if (count($ideaResponse['product_ids']) > $all) {
                    $idea->setHasMore(true);
                }
                break;
            case 2:
                $tag = $this->tagRepository->findById($ideaResponse['tag_ids'][0]);

                return new RedirectResponse($this->urlGenerator->generate('website_product_tag', [
                    'slug' => $tag->getSlug(),
                ]));
                break;
        }

        return $idea;
    }

    private function getSearchParams(ListParams $listParams)
    {
        $searchParams = [];
        $period = $listParams->getPeriod();

        if ($period->hasStart()) {
            $searchParams['time_from'] = $period->getStart()->getTimestamp();
        }

        if ($period->hasEnd()) {
            $searchParams['time_to'] = $period->getEnd()->getTimestamp();
        }

        if ($listParams->getTypeSlug()) {
            $searchParams['type_slug'] = $listParams->getTypeSlug();
        }

        $searchParams['exclude_type_ids'] = $listParams->getExcludeTypeIds() ?: null;

        if ($listParams->getTagIds()) {
            $searchParams['tags'] = $listParams->getTagIds();
        }

        if ($listParams->getCityId()) {
            $searchParams['city_id'] = $listParams->getCityId();
        }

        if ($listParams->getUserId() && $listParams->isPrivate()) {
            $searchParams['user_id'] = $listParams->getUserId();
            $searchParams['is_private'] = true;
        }

        if ($listParams->getExcludeIds()) {
            $searchParams['exclude'] = $listParams->getExcludeIds();
        }

        $searchParams['is_promo'] = $listParams->isPromo();

        return $searchParams;
    }
}
