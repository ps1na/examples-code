<?php

namespace App\Modules\Homepage_v3\Controller;

use App\Common\Controller\AbstractController;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

class AdminHomepageController extends AbstractController
{
    /**
     * AdminHomepageController constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct($app);
    }

    /**
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('@Homepage_v3/admin/index.html.twig');
    }
}
