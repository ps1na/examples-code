<?php

namespace App\Modules\Homepage_v3\Controller;

use App\Common\Controller\AbstractController;
use App\Modules\Homepage_v3\Model\Anchor;
use App\Modules\Homepage_v3\Repository\VideoParentRepository;
use App\Modules\Homepage_v3\Repository\VideoRepository;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminVideoParentController extends AbstractController
{
    /**
     * @var VideoParentRepository
     */
    private $homepageVideoParentRepository;

    /**
     * @var \App\Modules\Video\Repository\VideoRepository
     */
    private $videoRepository;

    /**
     * @var Anchor
     */
    private $anchor;

    /**
     * AdminVideoController constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->homepageVideoParentRepository = $app['module.homepage.video_parent.repository'];
        $this->videoRepository = $app['module.video.video_repository'];
        $this->anchor = $app['module.admin.anchor_link'];
    }

    /**
     * @return Response
     */
    public function indexAction()
    {
        $videos = $this->homepageVideoParentRepository->findAll();

        return $this->render('@Homepage_v3/admin/parent/parent_index.html.twig', [
            'videos' => $videos,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addItemAction(Request $request)
    {
        $video = $this->homepageVideoParentRepository->getBlank();
        $video->setVideo($this->videoRepository->findById((int)$request->get('video_id')));
        $video->setPosition(0);
        $this->homepageVideoParentRepository->save($video);

        return new JsonResponse([
            'code' => Response::HTTP_OK,
        ]);
    }

    /**
     * @param $id
     *
     * @return RedirectResponse
     */
    public function removeItemAction($id)
    {
        $this->homepageVideoParentRepository->delete($id);

        return new RedirectResponse($this->urlGenerator->generate('homepage.promoblock.admin.main').$this->anchor->getVideoParent());
    }
}
