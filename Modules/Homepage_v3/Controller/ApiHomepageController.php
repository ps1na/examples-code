<?php

namespace App\Modules\Homepage_v3\Controller;

use App\Common\Controller\AbstractController;
use App\Modules\Homepage_v3\Model\Anchor;
use App\Modules\Homepage_v3\Model\Idea;
use App\Modules\Homepage_v3\Model\IdeaType;
use App\Modules\Homepage_v3\Repository\NewsRepository;
use App\Modules\Homepage_v3\Repository\PlaybillRepository;
use App\Modules\Homepage_v3\Repository\ProjectRepository;
use App\Modules\Homepage_v3\Repository\PromoblockItemRepository;
use App\Modules\Homepage_v3\Repository\VideoParentRepository;
use App\Modules\Homepage_v3\Repository\VideoRepository;
use App\TlumApp;
use Repositories\IdiberiProductCategoryRepository;
use Repositories\IdiberiProductRepository;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiHomepageController extends AbstractController
{
    /**
     * @var mixed|Service\UserLocationDetector\UserLocationDetector
     */
    private $userLocationDetector;

    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * @var PromoblockItemRepository
     */
    private $promoblockItemRepository;

    /**
     * @var NewsRepository
     */
    private $newsHomepageRepository;

    /**
     * @var PlaybillRepository
     */
    private $playbillRepository;

    /**
     * @var VideoRepository
     */
    private $homepageVideoRepository;

    /**
     * @var VideoParentRepository
     */
    private $homepageVideoParentRepository;

    /**
     * @var IdiberiProductCategoryRepository
     */
    private $idiberiProductCategoryRepository;

    /**
     * @var IdiberiProductRepository
     */
    private $idiberiProductRepository;

    /**
     * @var Anchor
     */
    private $anchor;

    /**
     * ApiHomepageController constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->userLocationDetector = $app['user_location.detector'];
        $this->projectRepository = $app['module.homepage.project.repository'];
        $this->promoblockItemRepository = $app['module.homepage.promoblock.repository'];
        $this->newsHomepageRepository = $app['module.homepage.news.repository'];
        $this->playbillRepository = $app['module.homepage.playbill.repository'];
        $this->homepageVideoRepository = $app['module.homepage.video.repository'];
        $this->homepageVideoParentRepository = $app['module.homepage.video_parent.repository'];
        $this->idiberiProductCategoryRepository = $app['idiberi_product_category_repository'];
        $this->idiberiProductRepository = $app['idiberi_product_repository'];
        $this->anchor = new Anchor();
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function getformClubEvent(Request $request)
    {
        $typeEvent = $request->get('type_event');
        $cityId = $this->userLocationDetector->getCityId();
        $periodTo = '–';

        if ($request->get('version') === 'day') {
            $period = $request->get('period');
            $periodTo = '';
        } else {
            $period = $request->get('datepicker1');
            $periodTo .= $request->get('datepicker2');
        }

        if (!$typeEvent) {
            $string = '/club-events/?city_id='.$cityId.'&period='.$period.$periodTo;
        } else {
            $string = '/club-events/section/'.$typeEvent.'/?city_id='.$cityId.'&period='.$period.$periodTo;
        }

        return new RedirectResponse($string);
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function editPositionForProject(Request $request)
    {
        $anchor = $this->anchor->getProject();
        $this->updateItem($request, $this->projectRepository, 'project');

        return new RedirectResponse($this->urlGenerator->generate('homepage.promoblock.admin.main').$anchor);
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function editPositionForPromoblockItems(Request $request)
    {
        $anchor = $this->anchor->getPromoblock();
        $this->updateItem($request, $this->promoblockItemRepository, 'promoblockitem');

        return new RedirectResponse($this->urlGenerator->generate('homepage.promoblock.admin.main').$anchor);
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function editPositionForNews(Request $request)
    {
        $anchor = $this->anchor->getNews();
        $this->updateItem($request, $this->newsHomepageRepository, 'news');

        return new RedirectResponse($this->urlGenerator->generate('homepage.promoblock.admin.main').$anchor);
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function editPositionForPlaybill(Request $request)
    {
        $anchor = $this->anchor->getPlaybill();
        $this->updateItem($request, $this->playbillRepository, 'playbill');

        return new RedirectResponse($this->urlGenerator->generate('homepage.promoblock.admin.main').$anchor);
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function editPositionForVideo(Request $request)
    {
        $anchor = $this->anchor->getVideo();
        $this->updateItem($request, $this->homepageVideoRepository, 'video');

        return new RedirectResponse($this->urlGenerator->generate('homepage.promoblock.admin.main').$anchor);
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function editPositionForVideoParent(Request $request)
    {
        $anchor = $this->anchor->getVideoParent();
        $this->updateItem($request, $this->homepageVideoParentRepository, 'video-parent');

        return new RedirectResponse($this->urlGenerator->generate('homepage.promoblock.admin.main').$anchor);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function showMoreProduct(Request $request)
    {
        $idCategory = '54edbc6e761b536d0a0041b8';
        $lastId = null;
        $limit = 8;
        $skip = (int)$request->get('skip');
        $categories = $this->idiberiProductCategoryRepository->getChildCategory($idCategory);
        $categories[] = $idCategory;
        $params['categories'] = $categories;
        $params['sort'] = 'available';
        $params['sort_order'] = -1;
        $params['sort2'] = true;
        $products = $this->idiberiProductRepository->find($lastId, $limit, $params, $skip)['items'];
        $html = '';
        foreach ($products as $product) {
            $html .= $this->twig->render('@Homepage_v3/web/product/product_card.html.twig', ['product' => $product, 'index' => '8']);
        }

        return new JsonResponse([
            'code' => JsonResponse::HTTP_OK,
            'html' => $html,
        ]);
    }

    /**
     * @param Request $request
     * @param         $repository
     * @param         $nameModel
     */
    private function updateItem(Request $request, $repository, $nameModel)
    {
        $itemsPosition = $request->request->all();
        $items = $repository->findAll();
        foreach ($items as $item) {
            $position = (int)$itemsPosition[$nameModel.$item->getId()];
            $item->setPosition($position);
            $repository->save($item);
        }
    }

    public function ideaProductsList(Application $app, Request $request)
    {
        $servName = 'stage2.';
        if ($app['_env'] === TlumApp::ENV_STAGING) {
            $servName = 'dev.';
        }
        if ($app['_env'] === TlumApp::ENV_PROD) {
            $servName = '';
        }
        if ($app['_env'] === TlumApp::ENV_STAGING_2) {
            $servName = 'stage2.';
        }
        // USE LOCAL REPOSITORY BECAUSE THIS IS FAST
        $id = $request->query->get('id');
        $url = 'https://'.$servName.'api.idiberi.ru/gifts/'.$id;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
        $response = curl_exec($ch);
        curl_close($ch);
        $ideaResponse = json_decode($response, true);
        $limit = 16;
        $skip = $request->query->get('offset');
        $reqPage = $request->query->get('page');
        $skip = $reqPage * $limit - $limit;
        $ideaType = new IdeaType($ideaResponse);
        $idea = new Idea($this->urlGenerator);
        $idea->setTitle($ideaResponse['title']);

        $i = 0;
        switch ($ideaType->whatTheType()) {
            case 0:
                $lastId = null;
                $params['category'] = $ideaResponse['category']['id'];
                $params['sort'] = 'modifiedAt';
                $params['sort_order'] = -1;
                $params['sort2'] = true;
                $products = $this->idiberiProductRepository->find($lastId, $limit, $params, $skip);
                foreach ($products['items'] as $product) {
                    $idea->addProduct($product);
                }
                if ($products['has_more']) {
                    $idea->setHasMore(true);
                }
                break;
            case 1:
                foreach ($ideaResponse['product_ids'] as $k => $productId) {
                    if ($skip <= $k && $i < $limit) {
                        $product = $this->idiberiProductRepository->findById($productId);
                        $idea->addProduct($product);
                        ++$i;
                    }
                }
                if (count($ideaResponse['product_ids']) > $skip + $limit) {
                    $idea->setHasMore(true);
                }
                break;
        }
        $content = '';

        $products = $app['service.get.store.product']->convert($idea->getProducts());
        $idea->setProducts($products);

        foreach ($idea->getProducts() as $product) {
            $content .= $this->render('@legacy/Website/Products/card_item.html.twig', [
                'product' => $product,
            ])->getContent();
        }

        return new JsonResponse([
            'code' => JsonResponse::HTTP_OK,
            'content' => $content,
            'hasMore' => $idea->isHasMore(),
        ]);
    }
}
