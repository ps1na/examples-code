<?php

namespace App\Modules\Homepage_v3\Controller;

use App\Common\Controller\AbstractController;
use App\Modules\Homepage_v3\Model\Anchor;
use App\Modules\Homepage_v3\Model\Playbill;
use App\Modules\Homepage_v3\Repository\PlaybillRepository;
use App\Modules\ResourcesLock\Model\ResourceLockFilter;
use App\Modules\ResourcesLock\Model\ResourcesLockType;
use App\Modules\ResourcesLock\Repository\ResourceLockRepository;
use Repositories\ClubEventRepository;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminPlaybillController extends AbstractController
{
    /**
     * @var PlaybillRepository
     */
    private $playbillRepository;

    /**
     * @var ResourceLockRepository
     */
    private $resourceLockRepository;

    /**
     * @var ClubEventRepository
     */
    private $clubEventRepository;

    /**
     * @var Anchor
     */
    private $anchor;

    /**
     * AdminPlaybillController constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->playbillRepository = $app['module.homepage.playbill.repository'];
        $this->resourceLockRepository = $app['module.resourceslock.repository'];
        $this->clubEventRepository = $app['module.club_event.repository'];
        $this->anchor = $app['module.admin.anchor_link'];
    }

    public function indexAction()
    {
        $resourceLockFilter = new ResourceLockFilter();
        $resourceLockFilter->setTypeId(ResourcesLockType::CLUB_EVENT);
        $resourcesLocksForClubEvents = $this->resourceLockRepository->findAllResourceLock($resourceLockFilter);
        $playbills = $this->playbillRepository->findAll();

        return $this->render('@Homepage_v3/admin/playbill/playbill_index.html.twig', [
            'resourcesLocks' => $resourcesLocksForClubEvents,
            'playbills' => $playbills,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addItemAction(Request $request)
    {
        $playbill = $this->playbillRepository->getBlank();
        $playbill->setPosition(0);
        $playbill->setClubEvent($this->clubEventRepository->findById((int)$request->get('event_id')));
        $this->playbillRepository->save($playbill);

        return new JsonResponse([
            'code' => Response::HTTP_OK,
        ]);
    }

    /**
     * @param $id
     *
     * @return RedirectResponse
     */
    public function removeItemAction($id)
    {
        $this->playbillRepository->delete($id);

        return new RedirectResponse($this->urlGenerator->generate('homepage.promoblock.admin.main').$this->anchor->getPlaybill());
    }
}
