<?php

namespace App\Modules\Homepage_v3\Controller;

use App\Common\Controller\AbstractController;
use App\Modules\Homepage_v3\Model\Anchor;
use App\Modules\Homepage_v3\Model\Project;
use App\Modules\Homepage_v3\Repository\ProjectRepository;
use Repositories\ImageRepository;
use Silex\Application;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminProjectController extends AbstractController
{
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * @var ImageRepository
     */
    private $imageRepository;

    /**
     * @var Anchor
     */
    private $anchor;

    /**
     * AdminProjectController constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->projectRepository = $app['module.homepage.project.repository'];
        $this->imageRepository = $app['image_repository'];
        $this->anchor = $app['module.admin.anchor_link'];
    }

    /**
     * @return Response
     */
    public function indexAction()
    {
        $projects = $this->projectRepository->findAll();

        return $this->render('@Homepage_v3/admin/project/project_index.html.twig', [
            'projects' => $projects,
        ]);
    }

    /**
     * @return Response
     */
    public function createForm()
    {
        $project = new Project();

        return $this->render('@Homepage_v3/admin/project/project_form_index.html.twig', [
            'project' => $project,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function createFormAction(Request $request)
    {
        $project = $this->extractDataFromRequest($request);
        $this->projectRepository->save($project);

        return new RedirectResponse($this->urlGenerator->generate('homepage.promoblock.admin.main').$this->anchor->getProject());
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function editFormAction(Request $request)
    {
        $project = $this->extractDataFromRequest($request);
        $this->projectRepository->save($project);

        return new RedirectResponse($this->urlGenerator->generate('homepage.promoblock.admin.main').$this->anchor->getProject());
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function editAction(Request $request)
    {
        $id = $request->attributes->get('id');
        $project = $this->projectRepository->findById($id);

        return $this->render('@Homepage_v3/admin/project/project_form_index.html.twig', [
            'project' => $project,
        ]);
    }

    /**
     * @param $id
     *
     * @return RedirectResponse
     */
    public function deleteAction($id)
    {
        $this->projectRepository->delete($id);

        return new RedirectResponse($this->urlGenerator->generate('homepage.promoblock.admin.main').$this->anchor->getProject());
    }

    /**
     * @param Request $request
     *
     * @return Project
     */
    private function extractDataFromRequest(Request $request)
    {
        $entity = $this->projectRepository->getBlank();
        $entity->setId((int)$request->get('id'));
        $entity->setTitle((string)$request->get('title'));
        $entity->setLink((string)$request->get('link'));
        $entity->setImage($this->imageRepository->findOneByUuid((string)$request->get('image_uuid')));
        $entity->setPosition((int)$request->get('position'));

        return $entity;
    }
}
