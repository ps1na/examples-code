<?php

namespace App\Modules\Homepage_v3\Controller;

use App\Common\Controller\AbstractController;
use App\Modules\Homepage_v3\Model\Anchor;
use App\Modules\Homepage_v3\Model\PromoblockItem;
use App\Modules\Homepage_v3\Repository\PromoblockItemRepository;
use Repositories\ImageRepository;
use Silex\Application;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminPromoblockController extends AbstractController
{
    /**
     * @var PromoblockItemRepository
     */
    private $promoblockItemRepository;

    /**
     * @var ImageRepository
     */
    private $imageRepository;

    /**
     * @var Anchor
     */
    private $anchor;

    /**
     * AdminPromoblockController constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->promoblockItemRepository = $app['module.homepage.promoblock.repository'];
        $this->imageRepository = $app['image_repository'];
        $this->anchor = $app['module.admin.anchor_link'];
    }
    /**
     * @return Response
     */
    public function indexAction()
    {
        $promoblockItems = $this->promoblockItemRepository->findAll();

        return $this->render('@Homepage_v3/admin/promoblock/promoblock_index.html.twig', [
            'promoblockItems' => $promoblockItems,
        ]);
    }

    /**
     * @return Response
     */
    public function createForm()
    {
        return $this->render('@Homepage_v3/admin/promoblock/promoblock_form_index.html.twig', [
            'promoblockItem' => $this->promoblockItemRepository->getBlank(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function formAction(Request $request)
    {
        $promoblockItem = $this->extractDataFromRequest($request);
        $this->promoblockItemRepository->save($promoblockItem);

        return new RedirectResponse($this->urlGenerator->generate('homepage.promoblock.admin.main').$this->anchor->getPromoblock());
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function editAction(Request $request)
    {
        $id = $request->attributes->get('id');
        $promoblockItem = $this->promoblockItemRepository->findById($id);

        return $this->render('@Homepage_v3/admin/promoblock/promoblock_form_index.html.twig', [
            'promoblockItem' => $promoblockItem,
        ]);
    }

    public function deleteAction($id)
    {
        $this->promoblockItemRepository->delete($id);

        return new RedirectResponse($this->urlGenerator->generate('homepage.promoblock.admin.main').$this->anchor->getPromoblock());
    }

    /**
     * @param Request $request
     *
     * @return PromoblockItem
     */
    private function extractDataFromRequest(Request $request)
    {
        $entity = $this->promoblockItemRepository->getBlank();

        $entity->setId((int)$request->get('id'));
        $entity->setTitle((string)$request->get('title'));
        $entity->setLink((string)$request->get('link'));
        $entity->setLinkType((bool)$request->get('link_type'));
        $entity->setText((string)$request->get('text'));
        $entity->setImage1($this->imageRepository->findOneByUuid((string)$request->get('image1_uuid')));
        $entity->setImage2($this->imageRepository->findOneByUuid((string)$request->get('image2_uuid')));
        $entity->setImage3($this->imageRepository->findOneByUuid((string)$request->get('image3_uuid')));
        $entity->setImage4($this->imageRepository->findOneByUuid((string)$request->get('image4_uuid')));
        $entity->setImage5($this->imageRepository->findOneByUuid((string)$request->get('image5_uuid')));
        $entity->setImage6($this->imageRepository->findOneByUuid((string)$request->get('image6_uuid')));
        $entity->setPosition((int)$request->get('position'));

        return $entity;
    }
}
