<?php

namespace App\Modules\Homepage_v3\Controller;

use App\Common\Controller\AbstractController;
use App\Modules\Homepage_v3\Model\Anchor;
use App\Modules\Homepage_v3\Repository\NewsRepository;
use App\Modules\ResourcesLock\Model\ResourceLockFilter;
use App\Modules\ResourcesLock\Model\ResourcesLockType;
use App\Modules\ResourcesLock\Repository\ResourceLockRepository;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminNewsController extends AbstractController
{
    /**
     * @var NewsRepository
     */
    private $homepageNewsRepository;

    /**
     * @var ResourceLockRepository
     */
    private $resourceLockRepository;

    /**
     * @var NewsRepository
     */
    private $newsRepository;

    /**
     * @var Anchor
     */
    private $anchor;

    /**
     * AdminNewsController constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->homepageNewsRepository = $app['module.homepage.news.repository'];
        $this->resourceLockRepository = $app['module.resourceslock.repository'];
        $this->newsRepository = $app['news_repository'];
        $this->anchor = $app['module.admin.anchor_link'];
    }

    /**
     * @return Response
     */
    public function indexAction()
    {
        $news = $this->homepageNewsRepository->findAll();

        $resourceLockFilter = new ResourceLockFilter();
        $resourceLockFilter->setTypeId(ResourcesLockType::NEWS);
        $resourcesLocksForNews = $this->resourceLockRepository->findAllResourceLock($resourceLockFilter);

        return $this->render('@Homepage_v3/admin/news/news_index.html.twig', [
            'news' => $news,
            'resourcesLocks' => $resourcesLocksForNews,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addItemAction(Request $request)
    {
        $news = $this->homepageNewsRepository->getBlank();
        $news->setPosition(0);
        $news->setNews($this->newsRepository->findById((int)$request->get('news_id')));
        $this->homepageNewsRepository->save($news);

        return new JsonResponse([
            'code' => Response::HTTP_OK,
        ]);
    }

    /**
     * @param $id
     *
     * @return RedirectResponse
     */
    public function removeItemAction($id)
    {
        $this->homepageNewsRepository->delete($id);

        return new RedirectResponse($this->urlGenerator->generate('homepage.promoblock.admin.main').$this->anchor->getNews());
    }
}
