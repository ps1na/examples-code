;$(function () {
    var $blocks = $('.js-videos-add-playlist');

    var onBlockUpdate = function () {
        var $items = $blocks.find('.js-item');

        $items.each(function () {
            var $item = $(this);
            var $action = $item.find('.js-action').first();
            var $addBtn = $('<a/>', {
                class: 'js-homepage-video-add-item btn btn-sm bg-olive',
                text: 'Добавить',
                data: {
                    id: parseInt($item.data('id'), 10)
                },
            });

            $action.empty().append($addBtn);
        });
    };

    var onClick = function (event) {
        var $btn = $(this);
        $.ajax({
            method: 'PUT',
            url: ADMIN_HOMEPAGE_VIDEO_ADD_ITEM_URL,
            data: {
                video_id: $btn.data('id'),
            },
        }).then(function (response) {
            if (response.code !== 200) {
                alert(response.errors.join('\n'));
            } else {
                var redirectStr = window.location.pathname + '#block-video';
                window.location.replace(redirectStr);
                window.location.reload();
            }
        });

        event.preventDefault();
    };

    $blocks.on('click', '.js-homepage-video-add-item', onClick);
    $blocks.on('update', onBlockUpdate);
});