;$(function () {
    var $blocks = $('.js-homepage-add-news');

    var onBlockUpdate = function () {
        var $items = $blocks.find('.js-item');

        $items.each(function () {
            var $item = $(this);
            var $action = $item.find('.js-action').first();
            var $addBtn = $('<a/>', {
                class: 'js-homepage-add-news-item btn btn-sm bg-olive',
                text: 'Добавить',
                data: {
                    id: parseInt($item.data('id'), 10)
                },
            });

            $action.empty().append($addBtn);
        });
    };

    var onClick = function (event) {
        var $btn = $(this);
        $.ajax({
            method: 'PUT',
            url: ADMIN_HOMEPAGE_NEWS_ADD_ITEM_URL,
            data: {
                news_id: $btn.data('id'),
            },
        }).then(function (response) {
            if (response.code !== 200) {
                alert(response.errors.join('\n'));
            } else {
                var redirectStr = window.location.pathname + '#block-news';
                window.location.replace(redirectStr);
                window.location.reload();
            }
        });

        event.preventDefault();
    };

    $blocks.on('click', '.js-homepage-add-news-item', onClick);
    $blocks.on('update', onBlockUpdate);
});
