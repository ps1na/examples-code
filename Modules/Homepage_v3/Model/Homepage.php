<?php

namespace App\Modules\Homepage_v3\Model;

class Homepage
{
    /**
     * @var PromoblockItem[]
     */
    private $promoblockItems = [];

    /**
     * @return PromoblockItem[]
     */
    public function getPromoblockItems()
    {
        return (array)$this->promoblockItems;
    }

    /**
     * @param PromoblockItem[] $promoblockItems
     */
    public function setPromoblockItems($promoblockItems)
    {
        $this->promoblockItems = [];

        foreach ($promoblockItems as $promoblockItem) {
            $this->addPromoblockItem($promoblockItem);
        }
        $this->promoblockItems = $promoblockItems;
    }

    /**
     * @param $promoblockItem
     */
    public function addPromoblockItem($promoblockItem)
    {
        $this->promoblockItems[] = $promoblockItem;
    }
}
