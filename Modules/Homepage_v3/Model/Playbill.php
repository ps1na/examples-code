<?php

namespace App\Modules\Homepage_v3\Model;

use Entities\ClubEventEntity;

class Playbill
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var ClubEventEntity
     */
    private $club_event;

    /**
     * @var int
     */
    private $position;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return ClubEventEntity
     */
    public function getClubEvent()
    {
        return $this->club_event;
    }

    /**
     * @param ClubEventEntity $club_event
     */
    public function setClubEvent($club_event)
    {
        $this->club_event = $club_event;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return bool
     */
    public function isExist()
    {
        return (bool)$this->getId();
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return (bool)$this->getClubEvent()->isPublished();
    }
}
