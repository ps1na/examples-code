<?php

namespace App\Modules\Homepage_v3\Model;

class Anchor
{
    private $project = '#block-project';

    private $promoblock = '#block-promoblock';

    private $news = '#block-news';

    private $playbill = '#block-playbill';

    private $video = '#block-video';

    private $videoParent = '#block-video-parent';

    /**
     * @return string
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @return string
     */
    public function getPromoblock()
    {
        return $this->promoblock;
    }

    /**
     * @return string
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * @return string
     */
    public function getPlaybill()
    {
        return $this->playbill;
    }

    /**
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * @return string
     */
    public function getVideoParent()
    {
        return $this->videoParent;
    }
}
