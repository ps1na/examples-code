<?php

namespace App\Modules\Homepage_v3\Model;

use Entities\NewsEntity;

class News
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var NewsEntity
     */
    private $news;

    /**
     * @var int
     */
    private $position;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return NewsEntity
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * @param NewsEntity $news
     */
    public function setNews($news)
    {
        $this->news = $news;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return bool
     */
    public function isExist()
    {
        return (bool)$this->getId();
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return (bool)$this->getNews()->isPublished();
    }
}
