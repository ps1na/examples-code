<?php

namespace App\Modules\Homepage_v3\Model;

use Symfony\Component\HttpFoundation\JsonResponse;

class IdeaType
{
    const CATEGORY_TYPE = 0;

    const HAND_TYPE = 1;

    const TAG_TYPE = 2;

    /**
     * @var JsonResponse
     */
    private $response;

    public function __construct($response)
    {
        $this->response = $response;
    }

    /**
     * @return string
     */
    public function whatTheType()
    {
        $type = '';
        if (isset($this->response['category'])) {
            $type = $this::CATEGORY_TYPE;
        }

        if (isset($this->response['tag_ids']) && count($this->response['tag_ids'])) {
            $type = $this::TAG_TYPE;
        }

        if (isset($this->response['product_ids']) && count($this->response['product_ids'])) {
            $type = $this::HAND_TYPE;
        }

        // Если пустой - то ошибка типа

        return $type;
    }
}
