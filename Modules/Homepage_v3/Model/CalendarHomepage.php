<?php

namespace App\Modules\Homepage_v3\Model;

class CalendarHomepage
{
    /**
     * @return array
     */
    public function getCalendar()
    {
        $from = new \DateTime('now');
        $to = new \DateTime();
        $to->add(new \DateInterval('P10D'));
        $period = new \DatePeriod($from, new \DateInterval('P1D'), $to);
        $dateWeek = array_map(
            function ($item) {
                $calendarItem = new CalendarItem();
                $calendarItem->setDayString($item->format('Y.m.d'));
                $calendarItem->setDay($item->format('d'));
                $calendarItem->setMonth($item->format('m'));
                $calendarItem->setYear($item->format('y'));
                $calendarItem->setDayName($this->getDay(
                    $calendarItem->getDay(),
                    $calendarItem->getMonth(),
                    $calendarItem->getYear()
                ));
                if ($calendarItem->getDayName() === 'сб' || $calendarItem->getDayName() === 'вс') {
                    $calendarItem->setHoliday(1);
                }

                return $calendarItem;
            },
            iterator_to_array($period)
        );

        return $dateWeek;
    }

    /**
     * @param $day
     * @param $mon
     * @param $year
     *
     * @return mixed
     */
    public function getDay($day, $mon, $year)
    {
        $days = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'];
        $day = (int)$day; //если день двухсимвольный и <10
        $mon = (int)$mon; //если месяц двухсимвольный и <10
        $a = (int)((14 - $mon) / 12);
        $y = $year - $a;
        $m = $mon + 12 * $a - 2;
        $d = (7000 + (int)($day + $y + (int)($y / 4) - (int)($y / 100) + (int)($y / 400) + (31 * $m) / 12)) % 7;

        return $days[$d];
    }
}
