<?php

namespace App\Modules\Homepage_v3\Model;

use Entities\IdiberiImage;
use Entities\IdiberiProductEntity;
use Symfony\Component\Routing\Generator\UrlGenerator;

class Idea
{
    const ROUTE_NAME = 'ideas.web.products';

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string|null
     */
    private $url;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var IdiberiImage
     */
    private $imageDestop;

    /**
     * @var IdiberiImage
     */
    private $imageAdaptive;

    /**
     * @var IdiberiImage
     */
    private $imageTablet;

    /**
     * @var int
     */
    private $position;

    /**
     * @var IdiberiProductEntity[]
     */
    private $products;

    /**
     * @var UrlGenerator
     */
    private $urlGenerator;

    /**
     * @var bool
     */
    private $hasMore;

    /**
     * Idea constructor.
     *
     * @param UrlGenerator $urlGenerator
     */
    public function __construct(UrlGenerator $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    public static function generateAbsoluteUrl(UrlGenerator $urlGenerator, $id)
    {
        return $urlGenerator->generate(self::ROUTE_NAME, [
            'id' => (string)$id,
        ], UrlGenerator::ABSOLUTE_URL);
    }

    /**
     * @return string
     */
    public function getAbsoluteUrl()
    {
        return self::generateAbsoluteUrl($this->urlGenerator, $this->getId());
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return IdiberiImage
     */
    public function getImageDestop()
    {
        return $this->imageDestop;
    }

    /**
     * @param IdiberiImage $imageDestop
     */
    public function setImageDestop($imageDestop)
    {
        $this->imageDestop = $imageDestop;
    }

    /**
     * @return IdiberiImage
     */
    public function getImageAdaptive()
    {
        return $this->imageAdaptive;
    }

    /**
     * @param IdiberiImage $imageAdaptive
     */
    public function setImageAdaptive($imageAdaptive)
    {
        $this->imageAdaptive = $imageAdaptive;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return IdiberiProductEntity[]
     */
    public function getProducts()
    {
        return (array)$this->products;
    }

    /**
     * @param array $products
     */
    public function setProducts(array $products = [])
    {
        $this->products = [];

        foreach ($products as $product) {
            $this->addProduct($product);
        }
    }

    /**
     * @param IdiberiProductEntity $product
     */
    public function addProduct(IdiberiProductEntity $product)
    {
        $this->products[] = $product;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return bool
     */
    public function isHasMore()
    {
        return $this->hasMore;
    }

    /**
     * @param bool $hasMore
     */
    public function setHasMore($hasMore)
    {
        $this->hasMore = $hasMore;
    }

    /**
     * @return IdiberiImage
     */
    public function getImageTablet()
    {
        return $this->imageTablet;
    }

    /**
     * @param IdiberiImage $imageTablet
     */
    public function setImageTablet($imageTablet)
    {
        $this->imageTablet = $imageTablet;
    }
}
