<?php
/**
 * Created by PhpStorm.
 * User: Marakyschev
 * Date: 03.06.17
 * Time: 18:50.
 */

namespace App\Modules\Homepage_v3\Model;

use Entities\ImageEntity;

class Project
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $link;

    /**
     * @var ImageEntity
     */
    private $image;

    /**
     * @var int
     */
    private $position;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return ImageEntity
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param ImageEntity $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return bool
     */
    public function isExist()
    {
        return (bool)$this->getId();
    }
}
