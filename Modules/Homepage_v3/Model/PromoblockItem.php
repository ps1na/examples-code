<?php

namespace App\Modules\Homepage_v3\Model;

use Entities\ImageEntity;

class PromoblockItem
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $link;

    /**
     * @var bool
     */
    private $linkType;

    // FIXME: Создать массив с картинками

    /**
     * @var ImageEntity
     */
    private $image1;

    /**
     * @var ImageEntity
     */
    private $image2;

    /**
     * @var ImageEntity
     */
    private $image3;

    /**
     * @var ImageEntity
     */
    private $image4;

    /**
     * @var ImageEntity
     */
    private $image5;

    /**
     * @var ImageEntity
     */
    private $image6;

    /**
     * @var int
     */
    private $position;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return bool
     */
    public function isLinkType()
    {
        return $this->linkType;
    }

    /**
     * @param bool $linkType
     */
    public function setLinkType($linkType)
    {
        $this->linkType = $linkType;
    }

    /**
     * @return ImageEntity
     */
    public function getImage1()
    {
        return $this->image1;
    }

    /**
     * @param ImageEntity $image1
     */
    public function setImage1($image1)
    {
        $this->image1 = $image1;
    }

    /**
     * @return ImageEntity
     */
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * @param ImageEntity $image2
     */
    public function setImage2($image2)
    {
        $this->image2 = $image2;
    }

    /**
     * @return ImageEntity
     */
    public function getImage3()
    {
        return $this->image3;
    }

    /**
     * @param ImageEntity $image3
     */
    public function setImage3($image3)
    {
        $this->image3 = $image3;
    }

    /**
     * @return ImageEntity
     */
    public function getImage4()
    {
        return $this->image4;
    }

    /**
     * @param ImageEntity $image4
     */
    public function setImage4($image4)
    {
        $this->image4 = $image4;
    }

    /**
     * @return ImageEntity
     */
    public function getImage5()
    {
        return $this->image5;
    }

    /**
     * @param ImageEntity $image5
     */
    public function setImage5($image5)
    {
        $this->image5 = $image5;
    }

    /**
     * @return ImageEntity
     */
    public function getImage6()
    {
        return $this->image6;
    }

    /**
     * @param ImageEntity $image6
     */
    public function setImage6($image6)
    {
        $this->image6 = $image6;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isExist()
    {
        return (bool)$this->getId();
    }
}
