<?php

namespace App\Modules\Homepage_v3\Model;

class CalendarItem
{
    /**
     * @var string
     */
    private $dayString;

    /**
     * @var int
     */
    private $day;

    /**
     * @var int
     */
    private $month;

    /**
     * @var string
     */
    private $dayName;

    /**
     * @var int
     */
    private $year;

    /**
     * @var bool
     */
    private $holiday;

    /**
     * @return string
     */
    public function getDayString()
    {
        return $this->dayString;
    }

    /**
     * @param string $dayString
     */
    public function setDayString($dayString)
    {
        $this->dayString = $dayString;
    }

    /**
     * @return int
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param int $day
     */
    public function setDay($day)
    {
        $this->day = $day;
    }

    /**
     * @return int
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @param int $month
     */
    public function setMonth($month)
    {
        $this->month = $month;
    }

    /**
     * @return string
     */
    public function getDayName()
    {
        return $this->dayName;
    }

    /**
     * @param string $dayName
     */
    public function setDayName($dayName)
    {
        $this->dayName = $dayName;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return bool
     */
    public function isHoliday()
    {
        return $this->holiday;
    }

    /**
     * @param bool $holiday
     */
    public function setHoliday($holiday)
    {
        $this->holiday = $holiday;
    }
}
