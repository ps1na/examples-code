<?php

namespace App\Modules\Homepage_v3\Model;

class Video
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \App\Modules\Video\Model\Video
     */
    private $video;

    /**
     * @var int
     */
    private $position;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \App\Modules\Video\Model\Video
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * @param \App\Modules\Video\Model\Video $video
     */
    public function setVideo($video)
    {
        $this->video = $video;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return bool
     */
    public function isExist()
    {
        return (bool)$this->getId();
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return (bool)$this->getVideo()->isPublished();
    }
}
