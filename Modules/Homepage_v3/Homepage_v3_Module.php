<?php

namespace App\Modules\Homepage_v3;

use App\Common\Module\Module;
use App\Modules\Homepage_v3\Controller\AdminHomepageController;
use App\Modules\Homepage_v3\Controller\AdminNewsController;
use App\Modules\Homepage_v3\Controller\AdminPlaybillController;
use App\Modules\Homepage_v3\Controller\AdminProjectController;
use App\Modules\Homepage_v3\Controller\AdminPromoblockController;
use App\Modules\Homepage_v3\Controller\AdminVideoController;
use App\Modules\Homepage_v3\Controller\AdminVideoParentController;
use App\Modules\Homepage_v3\Controller\ApiHomepageController;
use App\Modules\Homepage_v3\Controller\WebHomepageController;
use App\Modules\Homepage_v3\Model\Anchor;
use App\Modules\Homepage_v3\Repository\NewsRepository;
use App\Modules\Homepage_v3\Repository\PlaybillRepository;
use App\Modules\Homepage_v3\Repository\ProjectRepository;
use App\Modules\Homepage_v3\Repository\PromoblockItemRepository;
use App\Modules\Homepage_v3\Repository\VideoParentRepository;
use App\Modules\Homepage_v3\Repository\VideoRepository;
use Entities\IdiberiUserRoleEntity;
use Silex\Application;

class Homepage_v3_Module extends Module
{
    public function onRegister()
    {
        $app = $this->getApplication();
        $this->addTwigNamespace(__DIR__.'/Resources/views', 'Homepage_v3');

        $app['module.homepage.admin.controller'] = $app::share(function (Application $app) {
            return new AdminHomepageController($app);
        });

        $app['module.homepage.web.controller'] = $app::share(function (Application $app) {
            return new WebHomepageController($app);
        });

        $app['module.homepage.promoblock.admin.controller'] = $app::share(function (Application $app) {
            return new AdminPromoblockController($app);
        });

        $app['module.homepage.promoblock.repository'] = $app::share(function (Application $app) {
            return new PromoblockItemRepository(
                $app['pdo.family_club'],
                $app['image_repository']
            );
        });

        $app['module.homepage.project.admin.controller'] = $app::share(function (Application $app) {
            return new AdminProjectController($app);
        });

        $app['module.homepage.project.repository'] = $app::share(function (Application $app) {
            return new ProjectRepository(
                $app['pdo.family_club'],
                $app['image_repository']
            );
        });

        $app['module.homepage.news.admin.controller'] = $app::share(function (Application $app) {
            return new AdminNewsController($app);
        });

        $app['module.homepage.news.repository'] = $app::share(function (Application $app) {
            return new NewsRepository(
                $app['pdo.family_club'],
                $app['news_repository']
            );
        });

        $app['module.homepage.video.admin.controller'] = $app::share(function (Application $app) {
            return new AdminVideoController($app);
        });

        $app['module.homepage.video.repository'] = $app::share(function (Application $app) {
            return new VideoRepository(
                $app['pdo.family_club'],
                $app['module.video.video_repository'],
                $app['module.video.playlist_repository']
            );
        });

        $app['module.homepage.playbill.admin.controller'] = $app::share(function (Application $app) {
            return new AdminPlaybillController($app);
        });

        $app['module.homepage.playbill.repository'] = $app::share(function (Application $app) {
            return new PlaybillRepository(
                $app['pdo.family_club'],
                $app['module.club_event.repository']
            );
        });

        $app['module.homepage.video_parent.admin.controller'] = $app::share(function (Application $app) {
            return new AdminVideoParentController($app);
        });

        $app['module.homepage.video_parent.repository'] = $app::share(function (Application $app) {
            return new VideoParentRepository(
                $app['pdo.family_club'],
                $app['module.video.video_repository'],
                $app['module.video.playlist_repository']
            );
        });

        $app['module.homepage.api.controller'] = $app::share(function (Application $app) {
            return new ApiHomepageController($app);
        });

        $app['module.admin.anchor_link'] = $app::share(function (Application $app) {
            return new Anchor();
        });

        $this->adminRoutes();
        $this->webRoutes();
        $this->apiRoutes();
    }

    private function adminRoutes()
    {
        $admin = $this->getAdminControllers();

        /**
         * Страница просмотра настроек Главной страницы.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->get(
            '/homepage-v3/',
            'module.homepage.admin.controller:indexAction'
        )
            ->bind('homepage.promoblock.admin.main')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Страница формы создания элемента промоблока.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->get(
            '/homepage-v3/promoblock/create',
            'module.homepage.promoblock.admin.controller:createForm'
        )
            ->bind('homepage.promoblock.admin.createForm')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Страница создания элемента промоблока.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->post(
            '/homepage-v3/promoblock/create',
            'module.homepage.promoblock.admin.controller:formAction'
        )
            ->bind('homepage.promoblock.admin.createFormAction')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Страница формы редактирования элемента промоблока.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->get(
            '/homepage-v3/promoblock/edit/{id}',
            'module.homepage.promoblock.admin.controller:editAction'
        )
            ->assert('id', '\d+')
            ->bind('homepage.promoblock.admin.editForm')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Страница редактирования элемента промоблока.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->post(
            '/homepage-v3/promoblock/edit/{id}',
            'module.homepage.promoblock.admin.controller:formAction'
        )
            ->assert('id', '\d+')
            ->bind('homepage.promoblock.admin.editFormAction')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Удаление элемента промоблока.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->delete(
            '/homepage-v3/promoblock/delete/{id}',
            'module.homepage.promoblock.admin.controller:deleteAction'
        )
            ->assert('id', '\d+')
            ->bind('homepage.promoblock.admin.deleteAction')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Страница формы создания проекта.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->get(
            '/homepage-v3/project/create',
            'module.homepage.project.admin.controller:createForm'
        )
            ->bind('homepage.project.admin.createForm')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Страница создания проекта.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->post(
            '/homepage-v3/project/create',
            'module.homepage.project.admin.controller:createFormAction'
        )
            ->bind('homepage.project.admin.createFormAction')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Страница формы редактирования проекта.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->get(
            '/homepage-v3/project/edit/{id}',
            'module.homepage.project.admin.controller:editAction'
        )
            ->assert('id', '\d+')
            ->bind('homepage.project.admin.editForm')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Страница редактирования проекта.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->post(
            '/homepage-v3/project/edit/{id}',
            'module.homepage.project.admin.controller:editFormAction'
        )
            ->assert('id', '\d+')
            ->bind('homepage.project.admin.editFormAction')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Удаление проекта.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->delete(
            '/homepage-v3/project/delete/{id}',
            'module.homepage.project.admin.controller:deleteAction'
        )
            ->assert('id', '\d+')
            ->bind('homepage.project.admin.deleteAction')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Добавление элемента в новости.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->put(
            '/homepage-v3/news/additem',
            'module.homepage.news.admin.controller:addItemAction'
        )
            ->bind('homepage.news.admin.addItemAction')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Удаление элемента новости.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->delete(
            '/homepage-v3/news/delete/{id}',
            'module.homepage.news.admin.controller:removeItemAction'
        )
            ->assert('id', '\d+')
            ->bind('homepage.news.admin.removeItemAction')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Добавление элемента в Кино и Мультфильмы.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->put(
            '/homepage-v3/video/additem',
            'module.homepage.video.admin.controller:addItemAction'
        )
            ->bind('homepage.video.admin.addItemAction')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Удаление элемента Кино и мультфильмы.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->delete(
            '/homepage-v3/video/delete/{id}',
            'module.homepage.video.admin.controller:removeItemAction'
        )
            ->assert('id', '\d+')
            ->bind('homepage.video.admin.removeItemAction')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Добавление элемента в Детская афиша.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->put(
            '/homepage-v3/playbill/additem',
            'module.homepage.playbill.admin.controller:addItemAction'
        )
            ->bind('homepage.playbill.admin.addItemAction')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Удаление элемента Детская афиша.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->delete(
            '/homepage-v3/playbill/delete/{id}',
            'module.homepage.playbill.admin.controller:removeItemAction'
        )
            ->assert('id', '\d+')
            ->bind('homepage.playbill.admin.removeItemAction')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Добавление элемента в Советы родителям.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->put(
            '/homepage-v3/video-parent/additem',
            'module.homepage.video_parent.admin.controller:addItemAction'
        )
            ->bind('homepage.video_parent.admin.addItemAction')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Удаление элемента Советы родителям.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $admin->delete(
            '/homepage-v3/video-parent/delete/{id}',
            'module.homepage.video_parent.admin.controller:removeItemAction'
        )
            ->assert('id', '\d+')
            ->bind('homepage.video_parent.admin.removeItemAction')
            ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;
    }

    private function webRoutes()
    {
        $web = $this->getWebControllers();

        /**
         * Страница просмотра Главной страницы.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        // $web->get(
        //     '/v3/',
        //     'module.homepage.web.controller:indexAction'
        // )
        //     ->bind('homepage.web.main')
        // ;

        /**
         * Страница просмотра Идеи с товарами.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $web->get(
            '/products/ideas/{id}/',
            'module.homepage.web.controller:indexActionIdeasProducts'
        )
            ->assert('id', '[\w-]+')
            ->bind('ideas.web.products')
        ;
    }

    private function apiRoutes()
    {
        $api = $this->getApiControllers();

        /**
         * Обработка формы детской афиши (клубные события).
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $api->post(
            '/v1/homepage_v3/form-playbill/',
            'module.homepage.api.controller:getformClubEvent'
        )
            ->bind('homepage.api.getPlaybill')
            // ->secure(IdiberiUserRoleEntity::ROLE_USER)
        ;

        /**
         * Изменение позиций для проектов.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $api->post(
            '/v1/homepage_v3/edit-position/project/',
            'module.homepage.api.controller:editPositionForProject'
        )
            ->bind('homepage.api.editPositionProject')
            // ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Изменение позиций для промоблока.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $api->post(
            '/v1/homepage_v3/edit-position/promoblockitems/',
            'module.homepage.api.controller:editPositionForPromoblockItems'
        )
            ->bind('homepage.api.editPositionForPromoblockItems')
            // ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Изменение позиций для новостей.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $api->post(
            '/v1/homepage_v3/edit-position/news/',
            'module.homepage.api.controller:editPositionForNews'
        )
            ->bind('homepage.api.editPositionForNews')
            // ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Изменение позиций для детской афиши.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $api->post(
            '/v1/homepage_v3/edit-position/playbill/',
            'module.homepage.api.controller:editPositionForPlaybill'
        )
            ->bind('homepage.api.editPositionForPlaybill')
            // ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Изменение позиций для Кино и мультфильмы.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $api->post(
            '/v1/homepage_v3/edit-position/video/',
            'module.homepage.api.controller:editPositionForVideo'
        )
            ->bind('homepage.api.editPositionForVideo')
            // ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Изменение позиций для Советы родителям.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $api->post(
            '/v1/homepage_v3/edit-position/video-parent/',
            'module.homepage.api.controller:editPositionForVideoParent'
        )
            ->bind('homepage.api.editPositionForVideoParent')
            // ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Загрузить еще продукты.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $api->post(
            '/v1/homepage_v3/more-products/',
            'module.homepage.api.controller:showMoreProduct'
        )
            ->bind('homepage.api.showMoreProduct')
            // ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;

        /**
         * Загрузить еще продукты.
         */
        /** @noinspection PhpUndefinedMethodInspection */
        $api->get(
            '/v1/homepage_v3/ideas/product/',
            'module.homepage.api.controller:ideaProductsList'
        )
            ->bind('homepage.api.ideas.showmore')
            // ->secure('IS_AUTHENTICATED_REMEMBERED')
        ;
    }
}
