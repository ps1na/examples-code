<?php

namespace App\Modules\Homepage_v3\Repository;

use App\Modules\Homepage_v3\Exceptions\NewsNotFoundException;
use App\Modules\Homepage_v3\Model\News;
use KEIII\PdoWrapper\PdoQuery;
use KEIII\PdoWrapper\PdoWrapper;

class NewsRepository
{
    /**
     * @var PdoWrapper
     */
    private $pdo;

    /**
     * @var \Repositories\NewsRepository
     */
    private $newsRepository;

    /**
     * NewsRepository constructor.
     *
     * @param PdoWrapper                   $pdo
     * @param \Repositories\NewsRepository $newsRepository
     */
    public function __construct(PdoWrapper $pdo, \Repositories\NewsRepository $newsRepository)
    {
        $this->pdo = $pdo;
        $this->newsRepository = $newsRepository;
    }

    /**
     * @return News
     */
    public function getBlank()
    {
        return new News();
    }

    /**
     * @param array $data
     *
     * @return News
     */
    private function createEntity(array $data)
    {
        $news = $this->getBlank();
        if ($data['id'] !== 0) {
            $news->setId($data['id']);
        }
        $news->setNews($this->newsRepository->findById($data['news_id']));
        $news->setPosition($data['position']);

        return $news;
    }

    /**
     * @param $id
     *
     * @return News
     */
    public function findById($id)
    {
        $pdoQuery = new PdoQuery('SELECT * FROM homepage_news WHERE id = :id', [':id' => $id]);
        $data = $this->pdo->read($pdoQuery)->getFirst();
        if (!is_array($data)) {
            throw new NewsNotFoundException();
        }

        return $this->createEntity($data);
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $news = [];
        $pdoQuery = new PdoQuery('SELECT id FROM homepage_news ORDER BY position ASC');
        $rows = $this->pdo->read($pdoQuery)->asArray();

        foreach ($rows as $row) {
            $news[] = $this->findById($row['id']);
        }

        return $news;
    }

    /**
     * @param News $news
     */
    public function save(News $news)
    {
        $this->pdo->beginTransaction();
        $id = $news->isExist() ? $news->getId() : $this->pdo->lastInsertId();
        $pdoQuery = new PdoQuery('
        INSERT INTO homepage_news (
              id,
              news_id,
              position
            ) VALUES (
              :id,
              :news_id,
              :position
            ) ON DUPLICATE KEY UPDATE
              news_id = :news_id,
              position = :position
        ;',
            [
                ':id' => $id,
                ':news_id' => $news->getNews()->getId(),
                ':position' => $news->getPosition(),
            ]);
        $this->pdo->write($pdoQuery);
        $this->pdo->commit();
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $pdoQuery = new PdoQuery('DELETE FROM homepage_news WHERE id = :id', [':id' => $id]);
        $this->pdo->beginTransaction();
        $this->pdo->write($pdoQuery);
        $this->pdo->commit();
    }
}
