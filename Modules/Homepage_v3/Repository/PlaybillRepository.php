<?php

namespace App\Modules\Homepage_v3\Repository;

use App\Modules\Homepage_v3\Exceptions\EntityNotFoundException;
use App\Modules\Homepage_v3\Model\Playbill;
use KEIII\PdoWrapper\PdoQuery;
use KEIII\PdoWrapper\PdoWrapper;
use Repositories\ClubEventRepository;

class PlaybillRepository
{
    /**
     * @var PdoWrapper
     */
    private $pdo;

    /**
     * @var ClubEventRepository
     */
    private $clubEventRepository;

    /**
     * PlaybillRepository constructor.
     *
     * @param PdoWrapper          $pdo
     * @param ClubEventRepository $clubEventRepository
     */
    public function __construct(PdoWrapper $pdo, ClubEventRepository $clubEventRepository)
    {
        $this->pdo = $pdo;
        $this->clubEventRepository = $clubEventRepository;
    }

    /**
     * @return Playbill
     */
    public function getBlank()
    {
        return new Playbill();
    }

    /**
     * @param array $data
     *
     * @return Playbill
     */
    private function createEntity(array $data)
    {
        $playbill = $this->getBlank();
        if ($data['id'] !== 0) {
            $playbill->setId($data['id']);
        }
        $playbill->setClubEvent($this->clubEventRepository->findById($data['event_id']));
        $playbill->setPosition($data['position']);

        return $playbill;
    }

    /**
     * @param $id
     *
     * @return Playbill
     */
    public function findById($id)
    {
        $pdoQuery = new PdoQuery('SELECT * FROM homepage_playbill WHERE id = :id', [':id' => $id]);
        $data = $this->pdo->read($pdoQuery)->getFirst();
        if (!is_array($data)) {
            throw new EntityNotFoundException();
        }

        return $this->createEntity($data);
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $news = [];
        $pdoQuery = new PdoQuery('SELECT id FROM homepage_playbill ORDER BY position ASC');
        $rows = $this->pdo->read($pdoQuery)->asArray();

        foreach ($rows as $row) {
            $news[] = $this->findById($row['id']);
        }

        return $news;
    }

    /**
     * @param Playbill $playbill
     */
    public function save(Playbill $playbill)
    {
        $this->pdo->beginTransaction();
        $id = $playbill->isExist() ? $playbill->getId() : $this->pdo->lastInsertId();
        $pdoQuery = new PdoQuery('
        INSERT INTO homepage_playbill (
              id,
              event_id,
              position
            ) VALUES (
              :id,
              :event_id,
              :position
            ) ON DUPLICATE KEY UPDATE
              event_id = :event_id,
              position = :position
        ;',
            [
                ':id' => $id,
                ':event_id' => $playbill->getClubEvent()->getId(),
                ':position' => $playbill->getPosition(),
            ]);
        $this->pdo->write($pdoQuery);
        $this->pdo->commit();
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $pdoQuery = new PdoQuery('DELETE FROM homepage_playbill WHERE id = :id', [':id' => $id]);
        $this->pdo->beginTransaction();
        $this->pdo->write($pdoQuery);
        $this->pdo->commit();
    }
}
