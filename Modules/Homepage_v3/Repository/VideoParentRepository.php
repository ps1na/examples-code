<?php

namespace App\Modules\Homepage_v3\Repository;

use App\Modules\Homepage_v3\Exceptions\EntityNotFoundException;
use App\Modules\Homepage_v3\Model\Video;
use App\Modules\Video\Repository\VideoPlaylistRepository;
use KEIII\PdoWrapper\PdoQuery;
use KEIII\PdoWrapper\PdoWrapper;

class VideoParentRepository
{
    /**
     * @var PdoWrapper
     */
    private $pdo;

    /**
     * @var \App\Modules\Video\Repository\VideoRepository
     */
    private $videoRepository;

    /**
     * VideoParentRepository constructor.
     *
     * @param PdoWrapper                                    $pdo
     * @param \App\Modules\Video\Repository\VideoRepository $videoRepository
     * @param VideoPlaylistRepository                       $videoPlaylistRepository
     */
    public function __construct(
        PdoWrapper $pdo,
        \App\Modules\Video\Repository\VideoRepository $videoRepository,
        VideoPlaylistRepository $videoPlaylistRepository
    ) {
        $this->pdo = $pdo;
        $this->videoRepository = $videoRepository;
    }

    /**
     * @return Video
     */
    public function getBlank()
    {
        return new Video();
    }

    /**
     * @param array $data
     *
     * @return Video
     */
    private function createEntity(array $data)
    {
        $video = $this->getBlank();
        if (isset($data['id'])) {
            $video->setId($data['id']);
        }
        $video->setPosition($data['position']);
        $video->setVideo($this->videoRepository->findById($data['video_id']));

        return $video;
    }

    /**
     * @param $id
     *
     * @return Video
     */
    public function findById($id)
    {
        $pdoQuery = new PdoQuery('SELECT * FROM homepage_video_parent WHERE id = :id', [':id' => $id]);
        $data = $this->pdo->read($pdoQuery)->getFirst();
        if (!is_array($data)) {
            throw new EntityNotFoundException();
        }

        return $this->createEntity($data);
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $news = [];
        $pdoQuery = new PdoQuery('SELECT id FROM homepage_video_parent ORDER BY position ASC');
        $rows = $this->pdo->read($pdoQuery)->asArray();

        foreach ($rows as $row) {
            $news[] = $this->findById($row['id']);
        }

        return $news;
    }

    /**
     * @param Video $video
     */
    public function save(Video $video)
    {
        $this->pdo->beginTransaction();
        $id = $video->isExist() ? $video->getId() : $this->pdo->lastInsertId();
        $pdoQuery = new PdoQuery('
        INSERT INTO homepage_video_parent (
              id,
              position,
              video_id
            ) VALUES (
              :id,
              :position,
              :video_id
            ) ON DUPLICATE KEY UPDATE
              position = :position,
              video_id = :video_id
        ;',
            [
                ':id' => $id,
                ':position' => $video->getPosition(),
                ':video_id' => $video->getVideo()->getId(),
            ]);
        $this->pdo->write($pdoQuery);
        $this->pdo->commit();
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $pdoQuery = new PdoQuery('DELETE FROM homepage_video_parent WHERE id = :id', [':id' => $id]);
        $this->pdo->beginTransaction();
        $this->pdo->write($pdoQuery);
        $this->pdo->commit();
    }
}
