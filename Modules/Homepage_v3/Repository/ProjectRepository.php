<?php

namespace App\Modules\Homepage_v3\Repository;

use App\Modules\Homepage_v3\Exceptions\ProjectNotFoundException;
use App\Modules\Homepage_v3\Model\Project;
use KEIII\PdoWrapper\PdoQuery;
use KEIII\PdoWrapper\PdoWrapper;
use Repositories\ImageRepository;

class ProjectRepository
{
    /**
     * @var PdoWrapper
     */
    private $pdo;

    /**
     * @var ImageRepository
     */
    private $imageRepository;

    /**
     * ProjectRepository constructor.
     *
     * @param PdoWrapper      $pdo
     * @param ImageRepository $imageRepository
     */
    public function __construct(PdoWrapper $pdo, ImageRepository $imageRepository)
    {
        $this->pdo = $pdo;
        $this->imageRepository = $imageRepository;
    }

    /**
     * @return Project
     */
    public function getBlank()
    {
        $project = new Project();
        $project->setImage($this->imageRepository->getBlank());

        return $project;
    }

    /**
     * @param array $data
     *
     * @return Project
     */
    private function createEntity(array $data)
    {
        $project = $this->getBlank();
        if ($data['id'] !== 0) {
            $project->setId($data['id']);
        }
        $project->setTitle($data['title']);
        $project->setImage($this->imageRepository->findOneByUuid($data['image_uuid']));
        $project->setLink($data['link']);
        $project->setPosition($data['position']);

        return $project;
    }

    /**
     * @param $id
     *
     * @return Project
     */
    public function findById($id)
    {
        $pdoQuery = new PdoQuery('SELECT * FROM homepage_project WHERE id = :id', [':id' => $id]);
        $data = $this->pdo->read($pdoQuery)->getFirst();
        if (!is_array($data)) {
            throw new ProjectNotFoundException();
        }

        return $this->createEntity($data);
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $promoblockItems = [];
        $pdoQuery = new PdoQuery('SELECT id FROM homepage_project ORDER BY position ASC');
        $rows = $this->pdo->read($pdoQuery)->asArray();

        foreach ($rows as $row) {
            $promoblockItems[] = $this->findById($row['id']);
        }

        return $promoblockItems;
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $pdoQuery = new PdoQuery('DELETE FROM homepage_project WHERE id = :id', [':id' => $id]);
        $this->pdo->beginTransaction();
        $this->pdo->write($pdoQuery);
        $this->pdo->commit();
    }

    /**
     * @param Project $project
     */
    public function save(Project $project)
    {
        $this->pdo->beginTransaction();
        $id = $project->isExist() ? $project->getId() : $this->pdo->lastInsertId();
        $pdoQuery = new PdoQuery('
        INSERT INTO homepage_project (
              id,
              title,
              link,
              image_uuid,
              position
            ) VALUES (
              :id,
              :title,
              :link,
              :image_uuid,
              :position
            ) ON DUPLICATE KEY UPDATE
              title = :title,
              link = :link,
              image_uuid = :image_uuid,
              position = :position
        ;',
            [
                ':id' => $id,
                ':title' => $project->getTitle(),
                ':link' => $project->getLink(),
                ':image_uuid' => $project->getImage()->getUuid(),
                ':position' => $project->getPosition(),
            ]);
        $this->pdo->write($pdoQuery);
        $this->pdo->commit();
    }
}
