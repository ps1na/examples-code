<?php

namespace App\Modules\Homepage_v3\Repository;

use App\Modules\Homepage_v3\Exceptions\PromoblockItemNotFoundException;
use App\Modules\Homepage_v3\Model\PromoblockItem;
use KEIII\PdoWrapper\PdoQuery;
use KEIII\PdoWrapper\PdoWrapper;
use Repositories\ImageRepository;
use Repositories\ImageRepositoryInterface;

class PromoblockItemRepository
{
    /**
     * @var PdoWrapper
     */
    private $pdo;

    /**
     * @var ImageRepository
     */
    private $imageRepository;

    /**
     * PromoblockItemRepository constructor.
     *
     * @param PdoWrapper               $pdo
     * @param ImageRepositoryInterface $imageRepository
     */
    public function __construct(PdoWrapper $pdo, ImageRepositoryInterface $imageRepository)
    {
        $this->pdo = $pdo;
        $this->imageRepository = $imageRepository;
    }

    /**
     * @return PromoblockItem
     */
    public function getBlank()
    {
        $promoblockItem = new PromoblockItem();
        $promoblockItem->setImage1($this->imageRepository->getBlank());
        $promoblockItem->setImage2($this->imageRepository->getBlank());
        $promoblockItem->setImage3($this->imageRepository->getBlank());
        $promoblockItem->setImage4($this->imageRepository->getBlank());
        $promoblockItem->setImage5($this->imageRepository->getBlank());
        $promoblockItem->setImage6($this->imageRepository->getBlank());

        return $promoblockItem;
    }

    /**
     * @param array $data
     *
     * @return PromoblockItem
     */
    private function createEntity(array $data)
    {
        $promoblockItem = $this->getBlank();
        if ($data['id'] !== 0) {
            $promoblockItem->setId($data['id']);
        }
        $promoblockItem->setImage1($this->imageRepository->findOneByUuid($data['image1_uuid']));
        $promoblockItem->setImage2($this->imageRepository->findOneByUuid($data['image2_uuid']));
        $promoblockItem->setImage3($this->imageRepository->findOneByUuid($data['image3_uuid']));
        $promoblockItem->setImage4($this->imageRepository->findOneByUuid($data['image4_uuid']));
        $promoblockItem->setImage5($this->imageRepository->findOneByUuid($data['image5_uuid']));
        $promoblockItem->setImage6($this->imageRepository->findOneByUuid($data['image6_uuid']));
        $promoblockItem->setTitle($data['title']);
        $promoblockItem->setText($data['text']);
        $promoblockItem->setLink($data['link']);
        $promoblockItem->setLinkType($data['link_type']);
        $promoblockItem->setPosition($data['position']);

        return $promoblockItem;
    }

    /**
     * @param $id
     *
     * @throws PromoblockItemNotFoundException
     *
     * @return PromoblockItem
     */
    public function findById($id)
    {
        $pdoQuery = new PdoQuery('SELECT * FROM homepage_promoblock_item WHERE id = :id', [':id' => $id]);
        $data = $this->pdo->read($pdoQuery)->getFirst();
        if (!is_array($data)) {
            throw new PromoblockItemNotFoundException();
        }

        return $this->createEntity($data);
    }

    /**
     * @return PromoblockItem[]
     */
    public function findAll()
    {
        $promoblockItems = [];
        $pdoQuery = new PdoQuery('SELECT id FROM homepage_promoblock_item ORDER BY position ASC');
        $rows = $this->pdo->read($pdoQuery)->asArray();

        foreach ($rows as $row) {
            $promoblockItems[] = $this->findById($row['id']);
        }

        return $promoblockItems;
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $pdoQuery = new PdoQuery('DELETE FROM homepage_promoblock_item WHERE id = :id', [':id' => $id]);
        $this->pdo->beginTransaction();
        $this->pdo->write($pdoQuery);
        $this->pdo->commit();
    }

    /**
     * @param PromoblockItem $promoblockItem
     */
    public function save(PromoblockItem $promoblockItem)
    {
        $this->pdo->beginTransaction();
        $id = $promoblockItem->isExist() ? $promoblockItem->getId() : $this->pdo->lastInsertId();
        $pdoQuery = new PdoQuery('
        INSERT INTO homepage_promoblock_item (
              id,
              title,
              text,
              link,
              link_type,
              image1_uuid,
              image2_uuid,
              image3_uuid,
              image4_uuid,
              image5_uuid,
              image6_uuid,
              position
            ) VALUES (
              :id,
              :title,
              :text,
              :link,
              :link_type,
              :image1_uuid,
              :image2_uuid,
              :image3_uuid,
              :image4_uuid,
              :image5_uuid,
              :image6_uuid,
              :position
            ) ON DUPLICATE KEY UPDATE
              title = :title,
              text = :text,
              link = :link,
              link_type = :link_type,
              image1_uuid = :image1_uuid,
              image2_uuid = :image2_uuid,
              image3_uuid = :image3_uuid,
              image4_uuid = :image4_uuid,
              image5_uuid = :image5_uuid,
              image6_uuid = :image6_uuid,
              position = :position
        ;',
        [
            ':id' => $id,
            ':title' => $promoblockItem->getTitle(),
            ':text' => $promoblockItem->getText(),
            ':link' => $promoblockItem->getLink(),
            ':link_type' => (bool)$promoblockItem->isLinkType(),
            ':image1_uuid' => $promoblockItem->getImage1()->getUuid(),
            ':image2_uuid' => $promoblockItem->getImage2()->getUuid(),
            ':image3_uuid' => $promoblockItem->getImage3()->getUuid(),
            ':image4_uuid' => $promoblockItem->getImage4()->getUuid(),
            ':image5_uuid' => $promoblockItem->getImage5()->getUuid(),
            ':image6_uuid' => $promoblockItem->getImage6()->getUuid(),
            ':position' => $promoblockItem->getPosition(),
        ]);
        $this->pdo->write($pdoQuery);
        $this->pdo->commit();
    }
}
